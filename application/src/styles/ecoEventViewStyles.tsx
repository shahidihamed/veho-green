import { StyleSheet, Platform } from "react-native";

export const devColors = {
  black: "#000",
  blue: "dodgerblue",
  green: "#006400",
  orange: "orange",
  pink: "pink",
  red: "red",
  white: "#fff",
  yellow: "yellow",
  darkGreen: "#0C7644",
};

export const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  cardContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  cardContent: {
    height: 100,
    width: 100,
    marginTop: 40,
    borderRadius: 10,
    borderColor: devColors.darkGreen,
    flex: 1,
    backgroundColor: devColors.yellow,
    justifyContent: "center",
  },
  cardTitle: {
    justifyContent: "center",
    alignItems: "baseline",
    marginLeft: 20,
  },
  graph: {
    flex: 1,
    overflow: "visible",
  },
  main: {
    paddingBottom: 20,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  droidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});
