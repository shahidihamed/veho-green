import { Checkpoint } from './db/checkpoint.interfaces';
import { CarType, CarFuelType } from './db/car.interfaces';
import { EventsModel } from './db/event.interfaces';

export namespace Request {
  export interface Login {
    username?: string;
    email?: string;
    password: string;
  }

  export interface Register {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
  }

  export interface Update {
    username?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    password?: string;
    currentPassword?: string;
    points?: number;
    co2?: number;
  }

  export namespace Car {
    export interface Create {
      carType: CarType;
      fuelType: CarFuelType;
      registrationNumber: string;
    }

    export interface Update {
      carType?: CarType;
      fuelType?: CarFuelType;
      registrationNumber?: string;
    }
  }

  export namespace Event {
    export interface Create {
      date: EventsModel.EventDate;
      type: EventsModel.EventType;
      car?: string;
      checkpoint?: Checkpoint;
    }

    export interface Update {
      end?: string;
      tripLength?: number;
      points?: number;
      co2?: number;
    }
  }
}
