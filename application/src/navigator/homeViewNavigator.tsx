import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import tipsView from "../views/tipsView";
import homeView from "../views/homeView";
import campaignView from "../views/campaignView";
import settingsView from "../views/settingsView";
import profileSettingsView from "../views/profileSettingsView";

/**
 * 
 * Navigator for homeView. Keeps all of the views you can naviaget from the home page in order.
 * 
 */
const homeViewNavigator = ({ navigation }: any) => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ headerShown: false }}
        name="home"
        component={homeView}
      />
      <Stack.Screen name="Tips" component={tipsView} />
      <Stack.Screen
        name="Campaigns"
        component={campaignView}
        options={{ headerStyle: { backgroundColor: "#008000" },headerTintColor: '#fff', }}
      />
      <Stack.Screen name="profileSettingsView" options={{ title: "Profile" }} component={profileSettingsView} />
      <Stack.Screen name="Settings" component={settingsView} />
    </Stack.Navigator>
  );
};

export default homeViewNavigator;
