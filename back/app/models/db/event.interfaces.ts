import { Car as CarObject } from './car.interfaces';
import { Checkpoint } from './checkpoint.interfaces';
import { User } from './user.interfaces';
import { DataBaseObject } from './general.interfaces';

export namespace EventsModel {
  export interface Car extends BaseEvent {
    checkpoints: string[] | Checkpoint[];
    tripLength: number;
    car: string | CarObject;
    type: EventType.car;
  }

  export interface PublicTransport extends BaseEvent {
    checkpoints: string[] | Checkpoint[];
    tripLength: number;
    type: EventType.publicTransport;
  }

  export interface Walk extends BaseEvent {
    checkpoints: string[] | Checkpoint[];
    tripLength: number;
    type: EventType.walk;
  }

  export interface Shopping extends BaseEvent {
    type: EventType.shopping;
  }

  interface BaseEvent extends DataBaseObject {
    date: EventDate;
    user: string | User;
    points: number;
    co2: number;
  }

  export enum EventType {
    car = 'car',
    walk = 'walk',
    shopping = 'shopping',
    publicTransport = 'publicTransport'
  }

  export interface EventDate {
    start: Date | string;
    end?: Date | string;
  }
}
