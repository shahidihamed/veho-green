import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  header: {
    marginTop: "10%",
    fontSize: 35,
  },
  card: {
    borderRadius: 10,
  },
  avatarHeader: {
    textAlign: "center",
    alignItems: "center",
  },
  profileImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: "5%",
  },
  profileImageHolder: {},
  imageSlot: {
    position:"absolute",
    width: "30%",
    height: "100%",
  },
  imageSlot1: {
    height: "30%"
  },
  cardContainer: {
    height: 75,
    flex: 1,
    justifyContent: "space-around",
    flexDirection: "row",
  },
  cardContainerText: {
    fontSize: 18,
    textAlign: "center",
  },
  cardContainerActiveText: {
    fontSize: 15,
    color: "#006400",
    textAlign: "center",
  },
  modalContainerActiveText: {
    fontSize: 25,
    color: "green",
    textAlign: "center",
  },
  circle: {
    width: "18%",
    marginLeft: "5%",
    height: "100%",
    borderRadius: 50,
    backgroundColor: "#006400",
  },
  image: {
    alignItems: "center",
    paddingTop: "10%",
    paddingBottom: "10%",
    backgroundColor: "#006400"
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalHeader: {
    flex:1,
    flexDirection:"row",
    height:"0%",
    backgroundColor:"green"
  },
  modalCancelView: {
    width: "100%",
    alignItems:"flex-end"
  },
  modalInfoRows: {
    flex:1
  },
  modalViewLogo: {
    height: "40%",
    borderRadius: 15
  },
  modalInfoViewFirstRow: {
    flex:1,
    flexDirection:"row",
  },
  modalInfoViewSecondRow: {
    flex:1,
    flexDirection:"row",
  },
  modalActiveCarButton: {
    color:"white"
  },
  modalViewInfoSecondRow:{
    width:"40%",
    height:"30%",
    margin:"3%"
  },
  modalViewInfo: {
    width:"100%",
    height:"30%"
  },
  modalInfoViewFirstRowText: {
    fontSize:25
  },
  modalInfoViewSecondRowText: {
    fontSize:20
  },
  modalInfoViewThirdRow: {
    flex:1,
    flexDirection:"row",
    justifyContent:"space-around"
  },
  modalInfoViewThirdRowInfoWithLogo: {
    flex:1,
    flexDirection:"column",
    margin:1
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    width: "80%",
    height: "70%",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalViewFirstRow: {
    flex: 1,
    flexDirection:"row",
    justifyContent: "space-around",
    width: "100%" 
  },
  buttonTextStyle: {
    color:"green"
  },
  modalViewFirstRowButton: {
    borderColor: "green",
    textDecorationColor: "green"
  },
  modalViewHeader: {
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    fontSize: 25,
    textAlign: "center",
  },
  carListDetails: {
    flex: 1,
    justifyContent: "space-evenly",
    flexDirection: "column",
  },
});
