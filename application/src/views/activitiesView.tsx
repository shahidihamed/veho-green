import * as React from "react";
import { styles } from "../styles/activitiesViewStyles";
import { View, Text, Dimensions, Modal } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { devColors } from "../styles/statisticsViewStyles";
import { createStackNavigator } from "@react-navigation/stack";
import apiClient from "../helpers/apiClient";
import { useState } from "react";
import { Card } from "react-native-elements";
import {
  EventType,
  genRndCheckpoint,
  getRndIntFromInterval,
} from "../helpers/dataGenerator";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Checkpoint } from "../../../back/app/models/db/checkpoint.interfaces";
import MapView, { LatLng, Marker, MarkerProps } from "react-native-maps";
import * as Location from "expo-location";

export interface EcoEvent {
  car: string;
  checkpoints: any[];
  co2: number;
  date: {
    start: string;
  };
  id: string;
  points: number;
  tripLength: number;
  type: string;
  user: string;
}

interface EventLocation {
  longitude: number;
  latitude: number;
}

const dimensions = Dimensions.get("window").width;
const contentWidth = dimensions - 100;
const contentHeight = dimensions - dimensions / 20;

// Navigator for page stack
const ActivitiesStack = createStackNavigator();
export const ActivitiesViewNavigator = () => {
  return (
    <ActivitiesStack.Navigator>
      <ActivitiesStack.Screen name="Activities" component={activitiesView} />
    </ActivitiesStack.Navigator>
  );
};

// Main view
const activitiesView = ({ navigation }: any) => {
  const [eventsFetched, setEventsFetched] = useState<boolean>(false);
  const [monthsArranged, setmonthsArranged] = useState<boolean>(false);
  const [events, setEvents] = useState<EcoEvent[]>([]);
  const [months, setMonths] = useState<string[]>([]);
  const [eventModalVisible, setEventModalVisible] = useState<boolean>(false);
  const [eventPressed, setEventPressed] = useState<EcoEvent>();
  const [locationInfo, setLocation] = React.useState<Location.LocationObject>();
  const [selectedMonthIndex, setSelectedMonthIndex] = React.useState<number>();
  const [monthPressed, setMonthPressed] = React.useState<boolean>(false);
  const [markers, setMarker] = React.useState<MarkerProps>();
  const [userMarker, setUserMarker] = React.useState<MarkerProps>();
  const [
    checkpointCoords,
    setCheckpointCoords,
  ] = React.useState<EventLocation>();

  React.useEffect(() => {
    if (!monthsArranged) {
      setMonths(arrangeMonths());
      setmonthsArranged(true);
    }
    if (!eventsFetched) {
      const date = new Date();
      loadEvents(date.getFullYear(), date.getMonth() + 1);
      setEventsFetched(true);
    }
  });

  const monthName = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  // ===========================
  //         Functions
  // ===========================
  /**
   * Fetches events of a month and updates state.
   * @param year Value for year.
   * @param month Value for month.
   */
  async function loadEvents(year: number, month: number) {
    // fetch ecoEvents for month
    apiClient("/user/events", "GET", null, null, null, true)
      .then(async (res: any) => {
        const monthEvents = (res.events.filter(
          (e) =>
            new Date(e.date.start).getMonth() == month &&
            new Date(e.date.start).getFullYear() == year &&
            e.type != EventType.shopping
        ) as EcoEvent[])
          .sort(
            (a, b) =>
              new Date(b.date.start).valueOf() -
              new Date(a.date.start).valueOf()
          )
          .reverse();

        // adding dummy data to event for missing data.
        const eventsWithLocation: EcoEvent[] = [];
        for (const e of monthEvents) {
          eventsWithLocation.push(await addDummyData(e));
        }
        setEvents(eventsWithLocation);
      })
      .catch((err) => {
        alert(err);
      });
  }

  /**
   * Adds dummy data to event.
   * As fetched event from database does not contain some of required data
   * at the moment, such as location information, trip length or CO2 emission,
   * dummy data will be added to event after it is fetched for show casing
   * the event info in UI.
   */
  async function addDummyData(event: EcoEvent) {
    event.checkpoints = [
      await genRndCheckpoint(event.id, new Date(event.date.start)),
    ];
    const tripConstant =
      event.type == EventType.car || event.type == EventType.publicTransport
        ? getRndIntFromInterval(100, 2000)
        : 100;
    const co2constant = event.type == EventType.walk ? 1 : 10;
    event.tripLength = tripConstant * getRndIntFromInterval(10, 100);
    event.co2 =
      (co2constant *
        getRndIntFromInterval(1, 10) *
        Math.sqrt(event.tripLength)) /
      100;
    event.points = Math.floor(((1 / event.co2) * 10) ^ 4);
    if (event.type == EventType.shopping) delete event.tripLength;
    return event;
  }

  /**
   * Extracts the location information from EcoEvent object
   * @param event EcoEvent object
   */
  function getEventLocation(event: EcoEvent): EventLocation | undefined {
    let coords = { latitude: 0, longitude: 0 };
    if (event.checkpoints.length != 0) {
      const start: Checkpoint = event.checkpoints[0];
      coords.latitude = Number(start.latitude);
      coords.longitude = Number(start.longitude);
      return coords;
    }
    return undefined;
  }

  /**
   * Gets the length of trip of the EcoEvent
   * @param event EvoEvent object.
   */
  function getTripLength(event: EcoEvent) {
    return event.tripLength;
  }

  /**
   * Arranges the list of months in monthBar based on current date.
   */
  function arrangeMonths() {
    let arrangedMonths: string[] = [];
    let currentMonth = new Date().getMonth();
    for (let i = currentMonth + 1; i < monthName.length; i++) {
      arrangedMonths.push(`${monthName[i]} ${new Date().getFullYear() - 1}`);
    }
    for (const month of monthName) {
      arrangedMonths.push(month);
      if (month == monthName[currentMonth]) break;
    }
    return arrangedMonths;
  }

  /**
   * Formats date string value to dd.mm format.
   * @param dateStr String value of date.
   */
  function formatItemDate(dateStr: string): string {
    const date = new Date(dateStr);
    return [date.getDate(), date.getMonth()].join(".");
  }

  /**
   * Gets related name and icon for EcoEvent based on event type.
   * @param type String value of type of EcoEvent.
   */
  function getEventTypeData(
    type: string
  ): Partial<{ name: string; icon: string }> {
    switch (type) {
      case EventType.car:
        return { name: "Driving", icon: "car" };
      case EventType.walk:
        return { name: "Walking", icon: "walking" };
      case EventType.shopping:
        return { name: "Shopping", icon: "shopping-cart" };
      case EventType.publicTransport:
        return { name: "Pub. Transport", icon: "train" };
      default:
        return {};
    }
  }

  /**
   * Adds a marker on the map for given location.
   * @param coordinates Coordinates of location.
   */
  const addMarker: Function = async (coordinates: LatLng): Promise<any> => {
    const marker: MarkerProps = {
      coordinate: coordinates,
      description: "Destination",
      title: "To:",
    };
    setMarker(marker);
  };

  // ===========================
  //       UI Components
  // ===========================
  /**
   * The horizontal month bar on the top
   */
  const MonthBar = (): Partial<FlatList<any>> => {
    return (
      <FlatList
        style={styles.monthBar}
        horizontal={true}
        data={months}
        keyExtractor={(item, index) => index.toString()}
        getItemLayout={(data, index) => ({
          length: 50,
          offset: 50 * index,
          index,
        })}
        initialScrollIndex={1}
        renderItem={({ item, index }) => (
          <View
            style={[
              styles.monthTitle,
              selectedMonthIndex != index
                ? {}
                : {
                    backgroundColor: "#bff0bd",
                    borderRadius: 20,
                    width: 100,
                  },
            ]}
          >
            <TouchableOpacity
              onPress={() => {
                const today = new Date();
                const year =
                  item.length == 3
                    ? today.getFullYear()
                    : today.getFullYear() - 1;
                const month = monthName.indexOf(item.slice(0, 3)) + 1;
                loadEvents(year, month);
                setSelectedMonthIndex(index);
              }}
              style={styles.monthTitleTouchable}
            >
              <Text
                style={[
                  styles.monthTitleText,
                  selectedMonthIndex != index
                    ? {}
                    : {
                        fontWeight: "bold",
                      },
                ]}
              >
                {item}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      />
    );
  };

  /**
   * The scrollable list of eco-events of selected month
   */
  const MonthEvents = () => {
    return (
      <FlatList
        scrollEnabled={true}
        contentContainerStyle={{ flexGrow: 1 }}
        style={styles.eventList}
        numColumns={1}
        data={events}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }: any) => EventItem(item)}
      />
    );
  };

  /**
   * The Event item in the list
   * @param item The EcoEvent object
   */
  const EventItem = (item: EcoEvent) => {
    return (
      <Card containerStyle={styles.card}>
        <TouchableOpacity
          style={styles.ecoEvent}
          onPress={() => {
            setEventPressed(item);
            setEventModalVisible(true);
            getEventLocation(item);
          }}
        >
          <View style={styles.itemDate}>
            <Text>{formatItemDate(item.date.start)}</Text>
          </View>

          <View style={styles.itemType}>
            <Text style={{ textAlign: "center" }}>
              {getEventTypeData(item.type).name}
            </Text>
          </View>
          <View style={styles.itemIconView}>
            <FontAwesome5Icon
              name={getEventTypeData(item.type).icon!}
              size={35}
              color={devColors.green}
            />
          </View>
        </TouchableOpacity>
      </Card>
    );
  };

  /**
   * The EcoEvent detail modal
   */
  const EventDetailModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={eventModalVisible}
        onRequestClose={() => {
          setEventModalVisible(false);
        }}
      >
        {eventPressed ? (
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Card containerStyle={[styles.card, { elevation: 10 }]}>
              <View style={{ marginLeft: 20 }}>
                <FontAwesome5Icon
                  name={getEventTypeData(eventPressed!.type).icon!}
                  size={100}
                  color={devColors.green}
                />
              </View>

              <View
                style={{
                  width: "100%",
                  height: "80%",
                  borderColor: devColors.darkGreen,
                  borderRadius: 10,
                  borderStyle: "solid",
                  borderWidth: 2,
                  alignItems: "center",
                }}
              >
                <View
                  nativeID="event-name"
                  style={{
                    width: contentWidth - 20,
                    flex: 1,
                    justifyContent: "center",
                    paddingLeft: 10,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      color: devColors.green,
                      fontWeight: "bold",
                    }}
                  >
                    {getEventTypeData(eventPressed!.type).name}
                  </Text>
                </View>
                <View
                  nativeID="event-point"
                  style={{
                    width: contentWidth - 20,
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#bff0bd",
                    borderRadius: 20,
                    marginLeft: 4,
                    marginRight: 4,
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "center",
                      paddingLeft: 10,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        marginLeft: 20,
                      }}
                    >
                      Eco points:
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <Text
                      style={{
                        textAlignVertical: "center",
                        fontSize: 20,
                        fontWeight: "bold",
                        marginRight: 10,
                        color: devColors.green,
                      }}
                    >
                      {eventPressed!.points}
                    </Text>
                    <MaterialCommunityIcons
                      name="leaf"
                      size={6 * (dimensions / 80)}
                      color={devColors.darkGreen}
                      style={{}}
                    />
                  </View>
                </View>
                <View
                  nativeID="event-distance"
                  style={{
                    width: contentWidth - 20,
                    flex: 1,
                    marginBottom: 10,
                    marginTop: 10,
                    alignItems: "center",
                    flexDirection: "row",
                  }}
                >
                  <Text
                    style={{
                      marginVertical: 10,
                      marginHorizontal: 10,
                      fontSize: 16,
                      marginLeft: 20,
                    }}
                  >
                    Trip length:
                  </Text>
                  <Text
                    style={{
                      marginVertical: 10,
                      marginHorizontal: 10,
                      fontSize: 16,
                      marginLeft: 20,
                    }}
                  >
                    {eventPressed.tripLength / 1000} km
                  </Text>
                </View>
                <View
                  nativeID="event-map"
                  style={{
                    width: contentWidth - 20,
                    flex: 1.8,
                  }}
                >
                  {EventLocationMap(eventPressed)}
                </View>
              </View>
            </Card>
          </View>
        ) : (
          <Card containerStyle={styles.card} />
        )}
      </Modal>
    );
  };

  const EventLocationMap = (event: EcoEvent) => {
    const coords = getEventLocation(event);
    return (
      <View style={styles.container}>
        {coords ? (
          <MapView
            style={styles.mapStyle}
            region={{
              latitude: coords.latitude,
              longitude: coords.longitude,
              latitudeDelta: 0.005,
              longitudeDelta: 0.005,
            }}
            focusable={true}
          >
            <Marker
              coordinate={{
                latitude: coords.latitude,
                longitude: coords.longitude,
              }}
              title={"Place of the event"}
              pinColor={devColors.green}
            />
          </MapView>
        ) : (
          <Text>No location provided!</Text>
        )}
      </View>
    );
  };

  return (
    <View style={styles.main}>
      {MonthBar()}
      {MonthEvents()}
      {EventDetailModal()}
    </View>
  );
};

export default activitiesView;
