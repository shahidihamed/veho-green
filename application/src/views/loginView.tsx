import * as React from 'react';
import {
  View,
  Image,
} from 'react-native';
import { styles } from '../styles/loginViewStyles';
import apiClient from '../helpers/apiClient';
import { Button, Card, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { storeData, retrieveData } from '../helpers/sessionStorage';
import { AuthContext } from '../context/context';

/**
 * 
 * @description Base login view
 * 
 */

const LoginView = ({navigation}: any) => {
  const [ started, setStarted ] = React.useState<boolean>(false);
  const [ username, setUsername ] = React.useState<string>();
  const [ password, setPassword ] = React.useState<string>();
  const {signIn} = React.useContext(AuthContext);

  const loginPressed: Function = async () => {
    const body = JSON.stringify({
      username: username,
      password: password,
    });

    try {
      let userToken = await apiClient('/login', 'POST', null, null, body,
          false);
      if (!userToken.token) {
        alert('Login failed!');
        return;
      }
      await storeData('token', userToken.token);
      signIn();
    } catch (error) {
      console.log(error);
      alert('Login failed!');
    }
  };

  React.useEffect(() => {
    const useEffectAsync: Function = async () => {
      try {
        let userToken = await retrieveData('token');
        if (userToken) {
          signIn();
        }
      } catch (e) {
        alert(e);
      }
    };
    useEffectAsync();
  }, []);

  return (
      <>
        <View style={styles.container}>
          <Image style={styles.image}
                 source={require('../../assets/logo.png')}/>
          <View style={styles.inputView}>
            <Input
                placeholder=" Username"
                leftIcon={<Icon name="user-circle-o" size={24} color="green"/>}
                onChangeText={(value) => {
                  setUsername(value);
                }}
            />
            <Input
                secureTextEntry={true}
                placeholder=" Password"
                leftIcon={<Icon name="lock" size={24} color="green"/>}
                onChangeText={(value) => {
                  setPassword(value);
                }}
            />
          </View>
          <View style={styles.loginButtons}>
            <Button
                title="Login"
                titleStyle={{color: 'green'}}
                buttonStyle={{borderColor: 'green'}}
                type="clear"
                onPress={() => {
                  loginPressed();
                }}
            />
            <View style={styles.buttonDivider}/>
            <Button
                title="Register"
                titleStyle={{color: 'green'}}
                buttonStyle={{borderColor: 'green'}}
                containerStyle={{marginLeft:60}}
                type="clear"
                onPress={() => {
                  navigation.navigate('Register');
                }}
            />
          </View>
        </View>
      </>
  );
};

export default LoginView;
