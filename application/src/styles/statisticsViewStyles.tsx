import { StyleSheet, Platform, View, Dimensions } from "react-native";
import { colors } from "react-native-elements";

export const devColors = {
  black: "#000",
  blue: "dodgerblue",
  green: "#006400",
  orange: "orange",
  pink: "pink",
  red: "red",
  white: "#fff",
  yellow: "yellow",
  darkGreen: "#0C7644",
};

export const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
  },
  cardContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 16,
  },
  cardTitle: {
    textAlign: "left",
    marginLeft: 20,
    fontSize: 20,
  },
  chart: {
    backgroundColor: devColors.white,
  },
  graph: {
    flex: 1,
    overflow: "visible",
  },
  main: {
    paddingBottom: 20,
    marginTop: 20,
  },
  droidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? 2 : 0,
  },
});
