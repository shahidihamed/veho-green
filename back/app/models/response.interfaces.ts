import { User as UserModel } from './db/user.interfaces';
import { Checkpoint as CheckpointModel } from './db/checkpoint.interfaces';
import { Car as CarObject } from './db/car.interfaces';
import { EventsModel } from './db/event.interfaces';
export namespace Response {
  export interface Error {
    error: string;
  }

  export interface Login {
    token: string;
  }

  export interface Register {
    token: string;
  }

  export namespace User {
    export interface Get {
      token: string;
      user: UserModel;
    }

    export interface Update {
      token: string;
      user: UserModel;
    }
    export interface Cars {
      token: string;
      cars: CarObject[];
    }

    export interface Events {
      token: string;
      events: EventsModel.Car[] | EventsModel.PublicTransport[] | EventsModel.Shopping[] | EventsModel.Walk[];
    }
  }

  export namespace Car {
    export interface Create {
      token: string;
      car: CarObject;
    }

    export interface Get {
      token: string;
      car: CarObject;
    }

    export interface Update {
      token: string;
      car: CarObject;
    }

    export interface Delete {
      token: string;
    }
  }

  export namespace Event {
    export interface Create {
      token: string;
      event: EventsModel.Car | EventsModel.PublicTransport | EventsModel.Walk | EventsModel.Shopping;
    }

    export interface Get {
      token: string;
      event: EventsModel.Car | EventsModel.PublicTransport | EventsModel.Walk | EventsModel.Shopping;
    }

    export interface Update {
      token: string;
      event: EventsModel.Car | EventsModel.PublicTransport | EventsModel.Walk | EventsModel.Shopping;
    }

    export interface Delete {
      token: string;
    }

    export namespace Checkpoint {
      export interface Create {
        token: string;
        checkpoint: CheckpointModel;
      }

      export interface Get {
        token: string;
        checkpoint: CheckpointModel[];
      }

      export interface GetOne {
        token: string;
        checkpoint: CheckpointModel;
      }

      export interface Update {
        token: string;
        checkpoint: CheckpointModel;
      }

      export interface Delete {
        token: string;
      }
    }
  }
}
