/**
 * This file contains functions for generating dummy data
 */

import apiClient from './apiClient';

export enum EventType {
  car = 'car',
  walk = 'walk',
  shopping = 'shopping',
  publicTransport = 'publicTransport'
}

/**
 * Generates given amount of dummy data.
 */
export async function generateDummyData(num: number, date?: Date) {
  for (let i = 0; i < num; i++) {
    await createDummyEvent(await prepareDummyEventPayload());
    console.log(' - created event #' + i);
    await sleep(200);
  }
}

/**
 * Generates acceptable random date
 */
export async function genRndDate(): Promise<Date> {
  const dateStr = [
    '2020',
    getRndIntFromInterval(1, 12).pad(),
    getRndIntFromInterval(1, 28).pad() ].join('-');
  console.log(' --- date: ' + dateStr);
  return new Date(dateStr);
}

/**
 * Returns a random number from given interval including min & max.
 */
export function getRndIntFromInterval(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Adds at '0' to the beginning of number.
 * @returns at least 2 digit.
 */
Number.prototype.pad = function(numOfDigits: number = 2): string {
  return this.toString().padStart(numOfDigits, '0').slice(numOfDigits * -1);
};
declare global {
  interface Number {
    pad: (numOfDigits?: number) => string;
  }
}

/**
 * Generates number of random geolocation points given a center and a radius.
 * Reference URL: http://goo.gl/KWcPE.
 * @param  {Object} center A JS object with lat and lng attributes.
 * @param  {number} radius Radius in meters.
 * @return {Object} The generated random points as JS object with lat and lng attributes.
 */
function generateRandomPoint(center, radius) {
  let x0 = center.lng;
  let y0 = center.lat;
  // Convert Radius from meters to degrees.
  let rd = radius / 111300;

  let u = Math.random();
  let v = Math.random();

  let w = rd * Math.sqrt(u);
  let t = 2 * Math.PI * v;
  let x = w * Math.cos(t);
  let y = w * Math.sin(t);

  let xp = x / Math.cos(y0);

  // Resulting point.
  return {lat: y + y0, lng: xp + x0};
}

/**
 * Generates random checkpoint object
 */
export async function genRndCheckpoint(eventId: string, date: Date) {
  const helsinki = {lat: 60.17, lng: 24.94};
  const location = generateRandomPoint(helsinki, 2500);
  return {
    event: eventId,
    longitude: location.lng,
    latitude: location.lat,
    date: date,
  };
}

/**
 * Gets random event type from EventType enum
 */
export async function getRndEventType() {
  const types = Object.keys(EventType);
  return types[getRndIntFromInterval(0, types.length - 1)];
}

/**
 * Gets random car object from user cars.
 */
export async function getRndCar() {
  let car: any = {};
  apiClient('/user/cars', 'GET', null, null, null, true).then((res: any) => {
    console.log('--- cars:');
    console.log(res.cars);
    const cars = res.cars;
    const index = getRndIntFromInterval(0, cars.length - 1);
    console.log('index: ' + index);
    car = cars[index];
    console.log('----- selected car: ');
    console.log(car);
    return car;
  }).catch((err) => {
    alert(err);
  });
  return car;
}

/**
 * Generates delay (to stay under the limit of API calls for free account)
 */
async function sleep(msDelay: number) {
  return new Promise((resolve) => setTimeout(resolve, msDelay));
}

// =====================
// Data generation calls
// =====================
export async function getEventData(eventId: string) {
  let event: any = {};
  const res: any = await apiClient('/event/', 'GET', null, eventId, null, true);
  console.log(`fetched event by id ${eventId}: ${res.event}`);
  event = res.event;
  return event;
}

export async function updateDummyEventData(eventId: string) {
  let event = await getEventData(eventId);
  const tripConstant = (
      event.type == EventType.car || event.type == EventType.publicTransport)
      ? getRndIntFromInterval(100, 2000) : 100;
  const eventData: any = {
    date: event.date,
    tripLength: getRndIntFromInterval(10, 100) * tripConstant,
    co2: event.type == EventType.walk ? 1 : 10,
  };
  eventData.co2 *= getRndIntFromInterval(1, 10) *
      Math.sqrt(eventData.tripLength) / 100;
  eventData.points = Math.floor(1 / eventData.co2 * 10 ^ 4);
  if (event.type == EventType.shopping) delete eventData.tripLength;
  const res: any = await apiClient('/event/', 'PUT', null, eventId, eventData,
      true);
  event = res.event;
  return event;
}

export async function createDummyEvent(data: any) {
  let event: any = {};
  console.log('event payload:');
  console.log(data);
  const res: any = await apiClient('/event/create', 'POST', null, null,
      JSON.stringify(data), true);
  console.log('create event res:');
  console.log(res);
  event = res.event;
  return event;
}

export async function prepareDummyEventPayload(date?: Date) {
  date = date ?? await genRndDate();
  let eventData: any = {
    date: {start: date},
    type: await getRndEventType(),
    car: (await getRndCar()).id,
    checkpoint: await genRndCheckpoint('-', date),
  };
  if (eventData.type != EventType.car) delete eventData.car;
  return eventData;
}



