import * as React from "react";
import { FontAwesome, MaterialCommunityIcons, AntDesign, Octicons } from "@expo/vector-icons";
import { styles } from "../styles/homeViewStyles"
import { StyleSheet, View, Text, Button, Dimensions, Image, Animated } from "react-native";
import { Card, ListItem, Icon } from "react-native-elements";
import { FlatList, ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import AnimatedProgressWheel from 'react-native-progress-wheel';
import { storeData , retrieveData } from "../helpers/sessionStorage";
import apiClient from "../helpers/apiClient";
import { User } from "./profileSettingsView";
import {
  Pedometer,
} from 'expo-sensors';
import { Car } from "../models/CarModel";

const dimensions = Dimensions.get('window').width;

/**
 * 
 * Base view after user has logged in. Has a own navigation system under ./navigator/homeViewNavigator.tsx
 *  
 */
const homeView = ({ navigation }: any) => {
  const [steps, setSteps] = React.useState<number>(0);
  const [user, setUser] = React.useState<User | null>(null);
  const [car, setCar] = React.useState<Car | null>(null);  

  const checkUser = async () => {
    try {
      const token = await retrieveData("token"); 
      if(!token) {
        alert("Error fetching token!")
      }
      let userInfo: any = await apiClient("/user", "GET", null,null, null, true);
      setUser(userInfo);
      if(!userInfo.user.activeCar) {
        setCar(null);
      } else {
        await storeData("activeCar", userInfo.user.activeCar);
        let carDetail: any = await apiClient("/car/" + userInfo.user.activeCar, "GET", null,null, null, true);
        setCar(carDetail.car);
      }
    } catch(e: any) {
      console.log(e);
    }
  }


  //Determinates the image in the front page
  const determinateImage = (carType?: string) => {
    switch (carType) {
      case "a-series": return require("../../assets/Aclass.png");
      case "b-series": return require("../../assets/Bclass.png");
      case "c-series": return require("../../assets/Cclass.png");
      case "s-series": return require("../../assets/Sclass.png");
      default: return require("../../assets/Aclass.png");
    }
  };
    
//adds listener to check when user is back in thi
 React.useEffect(() => {
  navigation.addListener('focus', async () => {
    await checkUser();
  });
}, []);

  return (
    <>
    <ScrollView>
      <View style={styles.home}> 
      <View style={styles.upperBar}>
      <View>
        <TouchableOpacity onPress={() => navigation.navigate("profileSettingsView")}>
          <Octicons name="person" size={40} style={styles.configFace}/>
        </TouchableOpacity> 
        </View>
        <View>
        <TouchableOpacity onPress={() => navigation.navigate("Settings")}>
          <FontAwesome
            name="gear"
            style={styles.configSettings}
            spin="true"
            size={40}
          />
          </TouchableOpacity> 
        </View>
        </View>
        <Animated.View style={styles.helloMessageContainer}>
          <Text style={styles.helloMessage}>Hello, {user?.user?.username}</Text>
        </Animated.View>
        <View style={styles.image}>
        <TouchableOpacity onPress={() => navigation.navigate("Car")}>
          { car != null ? <Image style={styles.imageSlot} resizeMode="contain" source={determinateImage(car?.carType)} /> : <Text style={{fontSize: 20, padding: 80}}>No active car selected!</Text> }        
          </TouchableOpacity> 
          </View>
          <Card containerStyle={styles.card}>
          <Card.Title>Statistics</Card.Title>
          <Card.Divider />
          <View style={styles.progres}>
            <View style={styles.progresSlot}>

              <Text style={styles.firstMeter}>
                {user?.user?.events?.length}
                </Text>
                <Text style={styles.secondParam}>
                  Recent trips
                </Text>
            </View>
            <View style={styles.progresSlot}>
              <Text style={styles.firstMeter}>
                {steps} 
                </Text>
                <Text style={styles.secondParam}>
                  Steps
                </Text>
            </View>
          </View>
        </Card>
        <Card containerStyle={styles.card}>
          <Card.Title>Daily goal</Card.Title>
          <Card.Divider />
          <View style={styles.progres}>
            <View style={styles.progresSlot}>
              <AnimatedProgressWheel size={100} width={18} 
                  color={"#006400"}
                  progress={45}
                  backgroundColor={"#ff0000"}
                  animateFromValue={0}
                  duration={3000}/>
              <Text>
                Todays progress for greener world
              </Text>
            </View>
            <View style={styles.progresSlot}>
              <AnimatedProgressWheel size={100} width={18} 
                  color={"#006400"}
                  progress={75}
                  backgroundColor={"#ff0000"}
                  animateFromValue={0}
                  duration={3000}/>
              <Text>
                This weeks journey to greener planet
              </Text>
            </View>
          </View>
        </Card>
      </View>
      <View style={styles.bottomCards}>
        <Card containerStyle={styles.bottomCardStyle1}>
        <TouchableOpacity onPress={() => 
          navigation.navigate("Campaigns")
          }>
          <View style={styles.bottomCardDetail}>
            <Text style={styles.bottomCardContent}>Find Campaigns</Text>
            <MaterialCommunityIcons name="sale" size={10 * (dimensions / 80)} color="white" />
          </View>
        </TouchableOpacity>
        </Card>
        <Card containerStyle={styles.bottomCardStyle2} >
          <Card.FeaturedSubtitle>
            <Text style={{color:"black"}}>Shop smarter when it comes to food. 
                                          Plan your meals in advance so that you only buy what you need, 
                                          and reuse leftovers in lunches to reduce your food wastage each week.
            </Text>
          </Card.FeaturedSubtitle>
        <TouchableOpacity onPress={() => 
          navigation.navigate("Tips")
          }>
        </TouchableOpacity>
        </Card>
      </View>
      </ScrollView>
    </>
  );
};

export default homeView;
