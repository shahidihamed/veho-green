import { Response } from './../models/response.interfaces';
import express from 'express';

export function checkFieldsInRequest(
  req: express.Request<any, any, any, any>,
  res: express.Response<any>,
  headers: string[] = [],
  queryFields: string[] = [],
  postBodyFields: string[] = []
): express.Response<any> | void {
  try {
    let errorText = '';
    let error = false;

    const headersOk = headers?.every((field) => Object.keys(req.headers).some((bKey) => bKey === field));
    if (!headersOk) {
      errorText = `Missing required fields in header: ${headers}, `;
      error = true;
    }

    const queryOk = queryFields?.every((field) => Object.keys(req.query).some((qKey) => qKey === field));
    if (!queryOk) {
      errorText += `Missing required fields in query: ${queryFields}, `;
      error = true;
    }

    const bodyOk = req.body
      ? postBodyFields?.every((field) => Object.keys(req.body).some((bKey) => bKey === field))
      : false;
    if (!bodyOk) {
      errorText += `Missing required fields in body: ${postBodyFields}`;
      error = true;
    }

    if (error) {
      return res.status(400).json({ error: errorText } as Response.Error);
    }
  } catch (error) {
    console.error('Error happened in checkFieldsInRequest');
    console.log(error);
    throw error;
  }
}
