import { DataBaseObject } from './general.interfaces';

export interface Checkpoint extends DataBaseObject {
  event: string;
  longitude: string;
  latitude: string;
  date: Date;
}
