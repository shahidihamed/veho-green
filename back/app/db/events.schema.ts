import mongoose, { Schema } from 'mongoose';

const eventSchema: Schema<any> = new mongoose.Schema({
  date: {
    start: { type: Date, required: true },
    end: { type: Date, required: false },
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  points: { type: Number, required: true },
  co2: { type: Number, required: true },
  type: { type: String, required: true }, // enum

  // Optional fields
  checkpoints: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Checkpoint',
      required: false,
    },
  ],
  tripLength: { type: Number, required: false },
  car: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Car',
    required: false,
  },
});

eventSchema.set('toJSON', {
  transform: (document, ret) => {
    ret.id = ret._id.toString();
    delete ret._id;
    delete ret.__v;
  },
});

export const Events = mongoose.model('Event', eventSchema);
