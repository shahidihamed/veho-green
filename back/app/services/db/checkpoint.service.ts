import { Events } from '../../db/events.schema';
import { Checkpoint } from '../../models/db/checkpoint.interfaces';
import { Checkpoints } from '../../db/checkpoints.schema';

export async function create(checkpoint: Checkpoint, eventId: string): Promise<Checkpoint> {
  try {
    const newCheckpoint: Checkpoint = (await Checkpoints.create(checkpoint)).toJSON();
    await Events.updateOne({ _id: eventId }, { $push: { checkpoints: newCheckpoint.id } });
    return newCheckpoint;
  } catch (error) {
    console.error('Error happened in createCar function');
    throw error;
  }
}

export async function getById(id: string): Promise<Checkpoint | undefined> {
  try {
    return (await Checkpoints.find({ _id: id }))[0]?.toJSON();
  } catch (error) {
    console.log(error);
    console.error('Error happened in getById function');
    throw error;
  }
}

export async function getAllEventsCheckpoints(eventId: string): Promise<Checkpoint[] | undefined> {
  try {
    return (await Events.find({ _id: eventId }).populate('checkpoints'))[0]?.toJSON().checkpoints;
  } catch (error) {
    console.error('Error happened in getAllEventsCheckpoints function');
    throw error;
  }
}

export async function remove(checkpointId: string): Promise<void> {
  try {
    const checkpoint: Checkpoint = (await Checkpoints.find({ _id: checkpointId }))[0]?.toJSON();

    await Events.updateOne({ _id: checkpoint.event }, { $pull: { checkpoints: checkpointId } });

    await Events.deleteOne({ _id: checkpointId });
  } catch (error) {
    console.log(error);
    console.error('Error happened in getById function');
    throw error;
  }
}
