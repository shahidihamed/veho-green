import { UserToken } from '../../models/token.interfaces';
import { EventsModel } from '../../models/db/event.interfaces';
import { Request } from '../../models/request.interfaces';
import { Response } from '../../models/response.interfaces';
import express from 'express';

import * as carService from '../../services/db/car.service';
import * as eventService from '../../services/db/event.service';
import * as tokenService from '../../services/token.service';

export async function createCarEvent(
  body: Request.Event.Create,
  token: string,
  res: express.Response<any>
): Promise<EventsModel.Car | express.Response<any>> {
  try {
    const decodedToken: UserToken = tokenService.decodeToken(token);

    const car = await carService.getById(body.car);

    // Checks if car exists
    if (!car) {
      return res.status(404).json({ error: "Car with given id wasn't found" } as Response.Error);
    }

    // Checks if user owns the car
    if (!car.users.some((userId) => userId.toString() === decodedToken.userId)) {
      return res.status(403).json({ error: 'You are not owner of the car' } as Response.Error);
    }

    const carEvent: EventsModel.Car = {
      car: car.id,
      checkpoints: body.checkpoint ? [body.checkpoint] : [],
      co2: 0,
      date: body.date,
      points: 0,
      tripLength: 0,
      type: EventsModel.EventType.car,
      user: decodedToken.userId,
    };

    const newEvent = await eventService.create(carEvent);

    return res.status(201).json({
      event: newEvent,
      token: tokenService.updateToken(token),
    } as Response.Event.Create);
  } catch (error) {
    console.log('Error happened in createCarEvent helper');
    throw error;
  }
}

export async function createPublicTransportEvent(
  body: Request.Event.Create,
  token: string,
  res: express.Response<any>
): Promise<EventsModel.PublicTransport | express.Response<any>> {
  try {
    const decodedToken: UserToken = tokenService.decodeToken(token);

    const publicTransportEvent: EventsModel.PublicTransport = {
      checkpoints: body.checkpoint ? [body.checkpoint] : [],
      co2: 0,
      date: body.date,
      points: 0,
      tripLength: 0,
      type: EventsModel.EventType.publicTransport,
      user: decodedToken.userId,
    };

    const newEvent = await eventService.create(publicTransportEvent);

    return res.status(201).json({
      event: newEvent,
      token: tokenService.updateToken(token),
    } as Response.Event.Create);
  } catch (error) {
    console.log('Error happened in createPublicTransportEvent helper');

    throw error;
  }
}

export async function createWalkEvent(
  body: Request.Event.Create,
  token: string,
  res: express.Response<any>
): Promise<EventsModel.Walk | express.Response<any>> {
  try {
    const decodedToken: UserToken = tokenService.decodeToken(token);

    const walkEvent: EventsModel.Walk = {
      checkpoints: body.checkpoint ? [body.checkpoint] : [],
      co2: 0,
      date: body.date,
      points: 0,
      tripLength: 0,
      type: EventsModel.EventType.walk,
      user: decodedToken.userId,
    };

    const newEvent = await eventService.create(walkEvent);

    return res.status(201).json({
      event: newEvent,
      token: tokenService.updateToken(token),
    } as Response.Event.Create);
  } catch (error) {
    console.log('Error happened in createWalkEvent helper');

    throw error;
  }
}

export async function createShoppingEvent(
  body: Request.Event.Create,
  token: string,
  res: express.Response<any>
): Promise<EventsModel.Walk | express.Response<any>> {
  try {
    const decodedToken: UserToken = tokenService.decodeToken(token);

    const shoppingEvent: EventsModel.Shopping = {
      co2: 0,
      date: body.date,
      points: 0,
      type: EventsModel.EventType.shopping,
      user: decodedToken.userId,
    };

    const newEvent = await eventService.create(shoppingEvent);

    return res.status(201).json({
      event: newEvent,
      token: tokenService.updateToken(token),
    } as Response.Event.Create);
  } catch (error) {
    console.log('Error happened in createShoppingEvent helper');

    throw error;
  }
}

export async function checkEventTypeExists(
  body: Request.Event.Create,
  res: express.Response<any>
): Promise<void | express.Response<any>> {
  try {
    let typeExists = false;
    for (const type in EventsModel.EventType) {
      if (type === ((body.type as unknown) as string)) {
        typeExists = true;
      }
    }
    if (!typeExists) {
      return res.status(400).json({ error: 'Event type must be valid' } as Response.Error);
    }
  } catch (error) {
    console.log('Error happened in checkEventTypeExists helper');
    throw error;
  }
}
