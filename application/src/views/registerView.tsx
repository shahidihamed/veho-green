
import * as React from 'react';
import {
  View,
  Image
} from 'react-native';
import { styles } from '../styles/registerViewStyles';
import apiClient from '../helpers/apiClient';
import { Button, Card, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { storeData, retrieveData } from '../helpers/sessionStorage';
import { AuthContext } from '../context/context';

/**
 * 
 * Register view used to sign up new users. After register user is redirected to home page
 * 
 */

const RegisterView = ({ navigation }: any) => {
  const [username, setUsername] = React.useState<string>();
  const [usernameError, setUsernameError] = React.useState<string | null>(null);
  const [password, setPassword] = React.useState<string>();
  const [passwordError, setPasswordError] = React.useState<string | null>(null);
  const [email, setEmail] = React.useState<string>();
  const [emailError, setemailError] = React.useState<string | null>(null);
  const [firstname, setFirstname] = React.useState<string>();
  const [firstnameError, setFirstnameError] = React.useState<string | null>(null);
  const [lastName, setlastName] = React.useState<string>();
  const [lastNameError, setlastNameError] = React.useState<string | null>(null);

  const { signUp } = React.useContext(AuthContext);

  const register: Function = async () => {
    const body = JSON.stringify({
        username:username,
        password:password,
        email:email,
        firstName:firstname,
        lastName:lastName
    });

    try {
      let userToken = await apiClient('/register', 'POST', null, null, body, false);
      if (!userToken.token) {
        alert('Register failed!');
        return;
      }
      await storeData('token', userToken.token);
      signUp();
    } catch (error) {
      console.log(error);
      alert('Register failed!');
    }
  };

  //Check all of the fields. Now only check if 2 or less characters
  const checkValue: Function = (value: string, field: string) => {
    switch(field){
        case "email": {if(value.length <= 2){setemailError("Email too short");} else {setemailError(null);}break;}
        case "password": {if(value.length <= 2){setPasswordError("Password too short");} else {setPasswordError(null);}break;}
        case "username": {if(value.length <= 2){setUsernameError("Username too short");} else {setUsernameError(null);}break;}
        case "firstname": {if(value.length <= 2){setFirstnameError("Firstname too short");} else {setFirstnameError(null);}break;}
        case "lastname": {if(value.length <= 2){setlastNameError("Lastname too short");} else {setlastNameError(null);}break;}
    }
  }

  return (
    <>
      <View style={styles.container}>
        <Image style={styles.image} source={require('../../assets/logo.png')} />
        <View style={styles.inputView}>
        <View style={styles.buttonDivider}>
          <Input
            placeholder='email'
            errorStyle={{color:'red'}}
            errorMessage={emailError != null ? emailError : ""}
            renderErrorMessage={emailError != null ? true : false}
            leftIcon={<Icon name='envelope' size={24} color='green' />}
            onChangeText={(value) => {
              setEmail(value);
              checkValue(value, "email");
            }}
          />
        </View>
          <Input
            secureTextEntry={true}
            errorStyle={{color:'red'}}
            errorMessage={passwordError != null ? passwordError : ""}
            renderErrorMessage={passwordError != null ? true : false}
            placeholder='password'
            leftIcon={<Icon name='lock' size={24} color='green' />}
            onChangeText={(value) => {
              setPassword(value);
              checkValue(value, "password");

            }}
          />
           <Input
            errorStyle={{color:'red'}}
            errorMessage={usernameError != null ? usernameError : ""}
            renderErrorMessage={usernameError != null ? true : false}
            placeholder='username'
            leftIcon={<Icon name='user' size={24} color='green' />}
            onChangeText={(value) => {
              setUsername(value);
              checkValue(value, "username");
            }}
          />
          <Input
            errorStyle={{color:'red'}}
            errorMessage={firstnameError != null ? firstnameError : ""}
            renderErrorMessage={firstnameError != null ? true : false}
            placeholder='firstname'
            leftIcon={<Icon name='user' size={24} color='green' />}
            onChangeText={(value) => {
              setFirstname(value);
              checkValue(value, "firstname");
            }}
          />
           <Input
            errorStyle={{color:'red'}}
            errorMessage={lastNameError != null ? lastNameError : ""}
            renderErrorMessage={lastNameError != null ? true : false}
            placeholder='lastname'
            leftIcon={<Icon name='user' size={24} color='green' />}
            onChangeText={(value) => {
              setlastName(value);
              checkValue(value, "lastname");
            }}
          />
          <View style={{marginTop:35}}>
          <Button style={styles.registerButtonStyle}
            title='Register'
            titleStyle={{color:'green'}}
            buttonStyle={{borderColor:'green'}}
            type='clear'
            onPress={() => {
              register();
            }}
          />
          <Button style={styles.loginButtonStyle}
            title="Allready registered? Login here"
            titleStyle={{color:"green"}}
            buttonStyle={{borderColor:"green"}}
            type="clear"
            onPress={() => {
              navigation.navigate("Login");
            }}
          />
          </View>
        </View>
      </View>
    </>
  );
};

export default RegisterView;
