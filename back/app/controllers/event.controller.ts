import { Request } from '../models/request.interfaces';
import { Response } from '../models/response.interfaces';

import * as eventService from '../services/db/event.service';
import * as userService from '../services/db/user.service';
import * as tokenService from '../services/token.service';

import { checkFieldsInRequest } from '../middlewares/fields.middleware';
import { checkToken } from '../middlewares/token.middleware';
import { checkUser } from '../middlewares/user.middleware';

import { Router } from 'express';
import { UserToken } from '../models/token.interfaces';
import { EventsModel } from '../models/db/event.interfaces';
import {
  createCarEvent,
  createWalkEvent,
  createPublicTransportEvent,
  createShoppingEvent,
  checkEventTypeExists,
} from './helpers/event.helpers';

const eventRouter = Router();

eventRouter.post('/create', checkToken, checkUser, async (req, res) => {
  const headers = req.headers;
  const query = req.query;
  const body: Request.Event.Create = req.body;

  try {
    const requestError = checkFieldsInRequest(req, res, undefined, undefined, ['date', 'type']);
    if (requestError) return requestError;

    const token = headers.authorization;

    // Check if event type is invalid
    const error = await checkEventTypeExists(body, res);
    if (error) return error;
   
    switch (body.type) {
      case EventsModel.EventType.car:
        return await createCarEvent(body, token, res);
      case EventsModel.EventType.walk:
        return await createWalkEvent(body, token, res);
      case EventsModel.EventType.publicTransport:
        return await createPublicTransportEvent(body, token, res);
      case EventsModel.EventType.shopping:
        return await createShoppingEvent(body, token, res);
    }
  } catch (error) {
    console.error('Error happened in /event/create');
    res.status(500).send(error);
  }
});

eventRouter.get('/:id', checkToken, checkUser, async (req, res) => {
  const headers = req.headers;
  const query = req.query;
  try {
    const token = headers.authorization;
    const eventId = req.params.id;
    const exploded: boolean = query.exploded === 'true' ? JSON.parse(query.exploded) : false;

    const event = exploded ? await eventService.getByIdExploded(eventId) : await eventService.getById(eventId);

    // Checks if event exists
    if (!event) {
      return res.status(404).json({ error: "Event with given id wasn't found" } as Response.Error);
    }

    return res.status(200).json({
      token: tokenService.updateToken(token),
      event,
    } as Response.Event.Get);
  } catch (error) {
    console.error('Error happened in /event/:id');
    res.status(500).send(error);
  }
});

eventRouter.put('/:id', checkToken, checkUser, async (req, res) => {
  const headers = req.headers;
  const query = req.query;
  const body: Request.Event.Update = req.body;

  try {
    const token = headers.authorization;
    const eventId = req.params.id;

    const event = await eventService.getById(eventId);

    const decodedToken: UserToken = tokenService.decodeToken(token);

    // Checks if event exists
    if (!event) {
      return res.status(404).json({ error: "Event with given id wasn't found" } as Response.Error);
    }

    // Checks if user owns the event
    if (event.user.toString() !== decodedToken.userId) {
      return res.status(403).json({ error: 'You are not owner of the event' } as Response.Error);
    }

    const eventObject = {
      date: {
        start: event.date.start,
        end: body.end,
      },
      tripLength: body.tripLength,
      points: body.points,
      co2: body.co2,
    };

    const updatedEvent = await eventService.update(eventId, eventObject);

    await userService.updateUserPoints(decodedToken.userId);

    return res.status(200).json({
      token: tokenService.updateToken(token),
      event: updatedEvent,
    } as Response.Event.Update);
  } catch (error) {
    console.error('Error happened in /event/:id');
    res.status(500).send(error);
  }
});

eventRouter.delete('/:id', checkToken, checkUser, async (req, res) => {
  const headers = req.headers;
  const query = req.query;

  try {
    const token = headers.authorization;
    const eventId = req.params.id;

    const event = await eventService.getById(eventId);

    const decodedToken: UserToken = tokenService.decodeToken(token);

    // Checks if event exists
    if (!event) {
      return res.status(404).json({ error: "Event with given id wasn't found" } as Response.Error);
    }

    // Checks if user owns the event
    if (event.user.toString() !== decodedToken.userId) {
      return res.status(403).json({ error: 'You are not owner of the event' } as Response.Error);
    }

    await eventService.remove(eventId);

    await userService.updateUserPoints(decodedToken.userId);

    return res.status(200).json({
      token: tokenService.updateToken(token),
    } as Response.Car.Delete);
  } catch (error) {
    console.error('Error happened in /event/:id');
    res.status(500).send(error);
  }
});

export { eventRouter };
