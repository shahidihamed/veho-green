export interface Car {
  carType?: string;
  fuelType?: string;
  registrationNumber?: string;
  id: string;
}
