import { Car, CarType, CarFuelType } from '../models/db/car.interfaces';
import { Request } from '../models/request.interfaces';
import { Response } from '../models/response.interfaces';

import * as carService from '../services/db/car.service';
import * as tokenService from '../services/token.service';

import { checkFieldsInRequest } from '../middlewares/fields.middleware';
import { checkToken } from '../middlewares/token.middleware';

import { Router } from 'express';
import { UserToken } from '../models/token.interfaces';

const carRouter = Router();

carRouter.post('/create', checkToken, async (req, res) => {
  const headers = req.headers;
  const query = req.query;
  const body: Request.Car.Create = req.body;

  try {
    const requestError = checkFieldsInRequest(req, res, undefined, undefined, [
      'fuelType',
      'carType',
      'registrationNumber',
    ]);
    if (requestError) return requestError;

    const token = headers.authorization;

    // Check if car type is invalid
    let typeExists = false;
    for (const type in CarType) {
      if (type === ((body.carType as unknown) as string)) {
        typeExists = true;
      }
    }
    if (!typeExists) {
      return res.status(400).json({ error: 'Car type must be valid' } as Response.Error);
    }

    // Check if fuel type is invalid
    typeExists = false;
    for (const type in CarFuelType) {
      if (type === ((body.fuelType as unknown) as string)) {
        typeExists = true;
      }
    }
    if (!typeExists) {
      return res.status(400).json({ error: 'Fuel type must be valid' } as Response.Error);
    }

    // Check if username is longer than 3 characters
    if (body.registrationNumber.length < 3) {
      return res
        .status(400)
        .json({ error: 'Registration number must be at least 3 characters long' } as Response.Error);
    }

    // Check if car already exists
    if (await carService.getByRegistrationNumber(body.registrationNumber)) {
      return res.status(409).json({ error: 'Car with given registration number already exists' } as Response.Error);
    }

    const decodedToken: UserToken = tokenService.decodeToken(token);

    let newCar: Car = {
      carType: body.carType,
      fuelType: body.fuelType,
      registrationNumber: body.registrationNumber,
      users: [decodedToken.userId],
    };

    newCar = await carService.create(newCar, decodedToken.userId);

    return res.status(200).json({
      token: tokenService.updateToken(token),
      car: newCar,
    } as Response.Car.Create);
  } catch (error) {
    console.error('Error happened in /car/create');
    res.status(500).send(error);
  }
});

carRouter.get('/:id', checkToken, async (req, res) => {
  const headers = req.headers;
  const query = req.query;
  try {
    const token = headers.authorization;
    const carId = req.params.id;

    const car = await carService.getById(carId);

    // Checks if car exists
    if (!car) {
      return res.status(404).json({ error: "Car with given id wasn't found" } as Response.Error);
    }

    return res.status(200).json({
      token: tokenService.updateToken(token),
      car,
    } as Response.Car.Get);
  } catch (error) {
    console.error('Error happened in /car/get/:id');
    res.status(500).send(error);
  }
});

carRouter.put('/:id', checkToken, async (req, res) => {
  const headers = req.headers;
  const query = req.query;
  const body: Request.Car.Update = req.body;

  try {
    const token = headers.authorization;
    const carId = req.params.id;

    const car = await carService.getById(carId);

    const decodedToken: UserToken = tokenService.decodeToken(token);

    // Checks if car exists
    if (!car) {
      return res.status(404).json({ error: "Car with given id wasn't found" } as Response.Error);
    }

    // Checks if user owns the car
    if (!car.users.some((userId) => userId.toString() === decodedToken.userId)) {
      return res.status(403).json({ error: 'You are not owner of the car' } as Response.Error);
    }

    // Check if car type is invalid
    let typeExists = false;
    for (const type in CarType) {
      if (type === ((body.carType as unknown) as string)) {
        typeExists = true;
      }
    }
    if (!typeExists) {
      return res.status(400).json({ error: 'Car type must be valid' } as Response.Error);
    }

    // Check if fuel type is invalid
    typeExists = false;
    for (const type in CarFuelType) {
      if (type === ((body.fuelType as unknown) as string)) {
        typeExists = true;
      }
    }
    if (!typeExists) {
      return res.status(400).json({ error: 'Fuel type must be valid' } as Response.Error);
    }

    const carObject = {
      fuelType: body.fuelType,
      carType: body.carType,
      registrationNumber: body.registrationNumber,
    };

    const updatedCar = await carService.update(carId, carObject);

    return res.status(200).json({
      token: tokenService.updateToken(token),
      car: updatedCar,
    } as Response.Car.Update);
  } catch (error) {
    console.error('Error happened in /car/update/:id');
    res.status(500).send(error);
  }
});

carRouter.delete('/:id', checkToken, async (req, res) => {
  const headers = req.headers;
  const query = req.query;

  try {
    const token = headers.authorization;
    const carId = req.params.id;

    const car = await carService.getById(carId);

    const decodedToken: UserToken = tokenService.decodeToken(token);

    // Checks if car exists
    if (!car) {
      return res.status(404).json({ error: "Car with given id wasn't found" } as Response.Error);
    }

    // Checks if user owns the car
    if (!car.users.some((userId) => userId.toString() === decodedToken.userId)) {
      return res.status(403).json({ error: 'You are not owner of the car' } as Response.Error);
    }

    await carService.remove(carId);

    return res.status(200).json({
      token: tokenService.updateToken(token),
    } as Response.Car.Delete);
  } catch (error) {
    console.error('Error happened in /car/update/:id');
    res.status(500).send(error);
  }
});

export { carRouter };
