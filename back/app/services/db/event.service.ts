import { Users } from '../../db/users.schema';
import { EventsModel } from '../../models/db/event.interfaces';
import { Events } from '../../db/events.schema';
import * as checkpointService from '../db/checkpoint.service';

export async function create(
  event: EventsModel.Car | EventsModel.PublicTransport | EventsModel.Shopping | EventsModel.Walk
): Promise<EventsModel.Car | EventsModel.PublicTransport | EventsModel.Shopping | EventsModel.Walk> {
  try {
    const newEvent: EventsModel.Car | EventsModel.PublicTransport | EventsModel.Shopping | EventsModel.Walk = (
      await Events.create(event)
    ).toJSON();
    await Users.updateOne({ _id: event.user }, { $push: { events: newEvent.id } });
    return newEvent;
  } catch (error) {
    console.error('Error happened in createCar function');
    throw error;
  }
}

export async function getById(
  id: string
): Promise<EventsModel.Car | EventsModel.PublicTransport | EventsModel.Shopping | EventsModel.Walk | undefined> {
  try {
    return (await Events.find({ _id: id }))[0]?.toJSON();
  } catch (error) {
    console.log(error);
    console.error('Error happened in getById function');
    throw error;
  }
}

export async function getByIdExploded(
  id: string
): Promise<EventsModel.Car | EventsModel.PublicTransport | EventsModel.Shopping | EventsModel.Walk | undefined> {
  try {
    return (await Events.find({ _id: id }).populate(['checkpoints']))[0]?.toJSON();
  } catch (error) {
    console.log(error);
    console.error('Error happened in getById function');
    throw error;
  }
}

export async function getAllUsersEvents(
  userId: string
): Promise<
  EventsModel.Car[] | EventsModel.PublicTransport[] | EventsModel.Shopping[] | EventsModel.Walk[] | undefined
> {
  try {
    return (
      await Users.find({ _id: userId }).populate([
        'events',
        {
          path: 'events',
          populate: {
            path: 'checkpoints',
          },
        },
      ])
    )[0]?.toJSON().events;
  } catch (error) {
    console.error('Error happened in getByUserId function');
    throw error;
  }
}

export async function getAllCarsEvents(carId: string): Promise<EventsModel.Car[] | undefined> {
  try {
    return (await Events.find({ car: carId })).map((car) => car.toJSON());
  } catch (error) {
    console.error('Error happened in getByUserId function');
    throw error;
  }
}

export async function update(
  eventId: string,
  fieldsToUpdate: { date?: EventsModel.EventDate; tripLength?: number }
): Promise<EventsModel.Car | EventsModel.PublicTransport | EventsModel.Shopping | EventsModel.Walk | undefined> {
  try {
    await Events.updateOne({ _id: eventId }, fieldsToUpdate);
    return (await Events.find({ _id: eventId }))[0]?.toJSON();
  } catch (error) {
    console.log(error);
    console.error('Error happened in update function');
    throw error;
  }
}

export async function remove(eventId: string): Promise<void> {
  try {
    const event:
      | EventsModel.Car
      | EventsModel.PublicTransport
      | EventsModel.Shopping
      | EventsModel.Walk = await getById(eventId);

    // Remove event from users events list
    await Users.updateOne({ _id: event.user }, { $pull: { events: event.id } });

    // Remove checkpoints related to event
    if (event.type !== EventsModel.EventType.shopping) {
      for (const checkpointId of event.checkpoints) {
        await checkpointService.remove(checkpointId.toString());
      }
    }

    // Remove checkpoint
    await Events.deleteOne({ _id: eventId });
  } catch (error) {
    console.log(error);
    console.error('Error happened in getById function');
    throw error;
  }
}
