import { User } from '../models/db/user.interfaces';
import { Request } from '../models/request.interfaces';
import { Response } from '../models/response.interfaces';

import * as userService from '../services/db/user.service';
import * as tokenService from '../services/token.service';
import * as passwordService from '../services/password.service';
import * as eventService from '../services/db/event.service';

import { checkFieldsInRequest } from '../middlewares/fields.middleware';
import { checkToken } from '../middlewares/token.middleware';
import { checkUser } from '../middlewares/user.middleware';

import { Router } from 'express';
import { UserToken } from '../models/token.interfaces';

const userRouter = Router();

userRouter.get('/', checkToken, checkUser, async (req, res) => {
  const headers = req.headers;
  const query = req.query;

  try {
    const token = headers.authorization;

    const decodedToken: UserToken = tokenService.decodeToken(token);

    const user = await userService.getById(decodedToken.userId);
    delete user.passwordHash;

    return res.status(200).json({
      token: tokenService.updateToken(token),
      user,
    } as Response.User.Get);
  } catch (error) {
    console.error('Error happened in /user/(get)');
    res.status(500).send(error);
  }
});

userRouter.put('/update', checkToken, checkUser, async (req, res) => {
  const headers = req.headers;
  const query = req.query;
  const body: Request.Update = req.body;

  try {
    const token = headers.authorization;

    const decodedToken: UserToken = tokenService.decodeToken(token);

    const user = await userService.getById(decodedToken.userId);

    // Verifying password
    const passwordIsVerified = passwordService.verifyPassword(body.currentPassword, user.passwordHash);

    // Password was not verified
    if (!passwordIsVerified) {
      return res.status(403).json({
        error: 'Wrong password',
      });
    }

    // Check if email is invalid
    if (body.email && !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(body.email)) {
      return res.status(400).json({ error: 'Email must be valid' } as Response.Error);
    }

    // Check if username is longer than 3 characters
    if (body.username && body.username.length < 3) {
      return res.status(400).json({ error: 'Username must be at least 3 characters long' } as Response.Error);
    }

    // Check if password is longer than 3 characters
    if (body.password && body.password.length < 3) {
      return res.status(400).json({ error: 'Password must be at least 3 characters long' } as Response.Error);
    }

    const fieldsToUpdate = {
      username: body.username,
      firstName: body.firstName,
      lastName: body.lastName,
      email: body.email,
      points: body.points,
      co2: body.co2,
      passwordHash: passwordService.encryptPassword(body.password),
    };

    //const updatedUser = await userService.updateUser(decodedToken.userId, fieldsToUpdate);
    return res.json({status: true});
    /*return res.status(200).json({
      token: tokenService.updateToken(token),
      user: updatedUser,
    } as Response.User.Update);*/
  } catch (error) {
    console.error('Error happened in /user/(get)');
    res.status(500).send(error);
  }
});

userRouter.patch('/activeCar/:id', checkToken, checkUser, async (req, res) => {
  const headers: any = req.headers;
  const id: String = req.params.id;
  const body: Request.Update = req.body;
  const token = headers.authorization;

  try {
    if(id.length == 0) {
      return res.status(400).json({ error: 'Id not valid' } as Response.Error);
    }

    const decodedToken: UserToken = tokenService.decodeToken(token);
    const updatedUser = await userService.updateUser(decodedToken.userId, {activeCar:id});

    return res.status(200).json({
      token: tokenService.updateToken(token),
      user: updatedUser,
    } as Response.User.Update);

  } catch(e) {
    console.log(e);
    console.error('Error happened in /activeCar/:id');
    res.status(500).send(e);
  } 
});

userRouter.get('/cars', checkToken, checkUser, async (req, res) => {
  const headers = req.headers;
  const query = req.query;

  try {
    const requestError = checkFieldsInRequest(req, res, undefined, undefined, []);
    if (requestError) return requestError;

    const token = headers.authorization;

    const decodedToken: UserToken = tokenService.decodeToken(token);
    const cars = await userService.getUsersCars(decodedToken.userId);
    console.log(cars);
    return res.status(200).json({
      token: tokenService.updateToken(token),
      cars,
    } as Response.User.Cars);
  } catch (error) {
    console.error('Error happened in /user/cars');
    res.status(500).send(error);
  }
});

userRouter.get('/events', checkToken, checkUser, async (req, res) => {
  const headers = req.headers;
  const query = req.query;

  try {
    const requestError = checkFieldsInRequest(req, res, undefined, undefined, []);
    if (requestError) return requestError;

    const token = headers.authorization;

    const decodedToken: UserToken = tokenService.decodeToken(token);

    const events = await eventService.getAllUsersEvents(decodedToken.userId);

    return res.status(200).json({
      token: tokenService.updateToken(token),
      events,
    } as Response.User.Events);
  } catch (error) {
    console.error('Error happened in /user/events');
    res.status(500).send(error);
  }
});

export { userRouter };
