import React, { useState } from "react";
import { View, Text, StyleSheet, FlatList, Modal } from "react-native";
import { ListItem, Button } from "react-native-elements";
import { createStackNavigator } from "@react-navigation/stack";
import { TouchableOpacity } from "react-native-gesture-handler";
import { styles } from "../styles/campaignViewStyles";
import { AntDesign } from '@expo/vector-icons';

const campaignView = ({ navigation }: any) => {
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [campaignSales, setCampaignSales] = useState<any[]>([]);
  const [startedLoading, setStartedLoading] = useState<any>(false);
  const [campaignIndex, setCampaignIndex] = useState<number>(0);
  const [userCash, setUserCash] = useState<number>(100);

  const [currentCampaign, setCurrentCampaign] = useState<any>(null);
  let popUp;

  React.useEffect(() => {
    if (!startedLoading) {
      setCampaignSales([
        {
          name: "Free coffee",
          partner: "R-kioski",
          price: 40,
          date: Date.now() + 4,
        },
        {
          name: "10 % from your next purchase",
          partner: "Finkino",
          price: 10,
          date: Date.now() + 4,
        },
      ]);
      setStartedLoading(true);
    }
  });

  const removeFromList = () => {
    setUserCash(userCash - currentCampaign.price);
    setCampaignSales(campaignSales.slice(campaignIndex + 1));
  };

  const removeTicket = () => {
    return new Promise((resolve, reject) => {
      //http request for backend
      setTimeout(() => {
        resolve();
      }, 1000);
    });
  };

  return (
    <>
      <View style={styles.ecoPoints}>
        <Text style={styles.points}>Your Eco points</Text>
        <Text style={styles.points}>{userCash}p</Text>
      </View>
      <View style={styles.list}>
        <FlatList
          data={campaignSales}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <ListItem bottomDivider style={styles.coupon}>
              <TouchableOpacity
                onPress={() => {
                  setModalVisible(true);
                  setCampaignIndex(index);
                  setCurrentCampaign(item);
                }}
              >
                <ListItem.Content style={styles.listContent}>
                  <ListItem.Title>{item.partner}</ListItem.Title>
                  <ListItem.Subtitle>{item.name}</ListItem.Subtitle>
                  <AntDesign style={styles.arrowRigth} name="rightcircleo" size={20} color="black" />
                </ListItem.Content>
              </TouchableOpacity>
            </ListItem>
          )}
        ></FlatList>
      </View>

      <Modal animationType="slide" transparent={true} visible={modalVisible} onRequestClose={() => {setModalVisible(false)}}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              {currentCampaign ? currentCampaign.partner : ""}
            </Text>
            <Text style={styles.modalText}>
              {currentCampaign ? currentCampaign.name : ""}
            </Text>
            <Button
              title="Redeem"
              onPress={async () => {
                removeTicket()
                  .then(() => {
                    setModalVisible(false);
                    removeFromList();
                  })
                  .catch(() => {
                    alert("Something went wrong!");
                  });
              }}
              type="clear"
            />
          </View>
        </View>
      </Modal>
    </>
  );
};

export default campaignView;
