import React, { useState } from "react";
import { View, Text, Image, Modal, Alert, BackHandler, TouchableOpacity } from "react-native";
import { BottomSheet, Button, Card, Input, ListItem } from "react-native-elements";
import {FlatList} from "react-native-gesture-handler";
import { styles } from "../styles/carViewStyles";
import apiClient from "../helpers/apiClient";
import { retrieveData } from "../helpers/sessionStorage";
import { AuthContext } from "../context/context";
import { MaterialIcons, Ionicons, Feather } from "@expo/vector-icons";
import { Car } from "../models/CarModel";

/**
 * 
 * View of your current cars. Can be found under nav bar cars.
 * Talks with the backends /user/cars to create, delete, 
 * Talks with the backends /user/activeCar to update active car, 
 * 
 * 
 */
const carView = () => {
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [carDetail, setcarDetailModalVisible] = useState<boolean>(false);
  const [carPressed, setCarPressed] = useState<Car>();
  const [cars, setCars] = useState<Car[]>([]);
  const [registrationNumber, setRegistrationNumber] = useState<string>();
  const [carType, setCarType] = useState<string>();
  const [activeCar, setActiveCar] = useState<string | null>();
  const [bottomSheetVisible, setBottomSheetVisible] = useState<boolean>(false);


  //Could be fetched from database in the future. List is now created according to our bacnend modal.
  const list: any[] = [
    { title: "a-series" },
    { title: "b-series" },
    { title: "c-series" },
    { title: "s-series" },
    {
      title: "Cancel",
      containerStyle: { backgroundColor: "red" },
      titleStyle: { color: "white" },
      onPress: () => setBottomSheetVisible(false),
    },
  ];

  React.useEffect((): void => {
      loadCars();
  },[]);

  
  const loadCars: Function = async () => {
    try {
      const car = await apiClient("/user/cars", "GET", null, null, null, true);
      setActiveCar(await retrieveData("activeCar"));
      setCars(car.cars);
    } catch (error) {
      alert(error);
    }
  };

  const changeToActiveCar: Function = async (id: string) => {
    try {
      await apiClient("/user/activeCar/" + id, "PATCH", null, null, null, true);
      setActiveCar(id);
    } catch (e: any) {
      console.log(e);
    }
  };

  const deleteCar: Function = async (id: string) => {
    try {
      await apiClient("/car/", "DELETE", null, id, null, true);
      loadCars();
      setcarDetailModalVisible(false);
    } catch (error) {
      alert(error);
    }
  };

  const setNewActiveCar: Function = (id: string) => {
    changeToActiveCar(id);
    setcarDetailModalVisible(false);                  
  }

  const newCar: Function = async () => {
    const body = JSON.stringify({
      fuelType: "diesel",
      carType: carType,
      registrationNumber: registrationNumber,
    });
    try {
      await apiClient("/car/create", "POST", null, null, body, true);
      loadCars();
      setModalVisible(false);
    } catch (error) {
      alert(error);
    }
  };

  const determinateImage = (carType?: string) => {
    switch (carType) {
      case "a-series":
        return require("../../assets/Aclass.png");
      case "b-series":
        return require("../../assets/Bclass.png");
      case "c-series":
        return require("../../assets/Cclass.png");
      case "s-series":
        return require("../../assets/Sclass.png");
      default:
        return require("../../assets/Aclass.png");
    }
  };

  return (
    <>
      <View>
        <Text style={styles.header}> Your cars</Text>
        {cars.length != 0 ? (
          <FlatList
            data={cars}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }: any) => (
              <Card containerStyle={{ marginTop: "2%", borderRadius: 15 }}>
                <TouchableOpacity
                  onPress={() => {
                    setcarDetailModalVisible(true);
                    setCarPressed(item);
                  }}
                >
                  <View style={styles.cardContainer}>
                    <View style={styles.circle} />
                    <Image
                      style={styles.imageSlot}
                      resizeMode="contain"
                      source={determinateImage(item.carType)}
                    />
                    <View style={styles.carListDetails}>
                      <Text style={styles.cardContainerText}>
                        {item.registrationNumber}
                      </Text>
                      <Text style={styles.cardContainerActiveText}>
                        {activeCar === item.id ? "Active" : null}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </Card>
            )}
          ></FlatList>
        ) : (
          <Text style={{ fontSize: 20, padding: 50 }}>
            No cars. Start by adding a car to your list.
          </Text>
        )}
        <Button
          titleStyle={{ color: "green" }}
          buttonStyle={{ borderColor: "green" }}
          title="Add car"
          onPress={() => setModalVisible(true)}
          type="clear"
        />

        {/* CREATE A COMPONENT FROM THE ITEM */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={carDetail}
          onRequestClose={() => {
            setcarDetailModalVisible(false);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
            <View style={styles.modalCancelView}>
            <TouchableOpacity onPress={() => {setcarDetailModalVisible(false)}}>
            <MaterialIcons name="cancel" size={24} color="green" />
            </TouchableOpacity>
            </View>
              <Image
                style={styles.imageSlot1}
                resizeMode="contain"
                source={determinateImage(carPressed?.carType)}/>
                <View style={styles.modalInfoRows}>
                <View style={styles.modalInfoViewFirstRow}>
                <View style={styles.modalViewInfo}>
                  <Text style={styles.modalInfoViewFirstRowText}>Mercedez-Benz</Text>
                  <Text style={styles.modalInfoViewFirstRowText}>{carPressed?.carType}</Text>
                </View>
                </View>
                <View style={styles.modalInfoViewSecondRow}>
                <View style={styles.modalViewInfoSecondRow}>
                  <Text style={styles.modalInfoViewSecondRowText}>{Math.floor(Math.random()* 100000)} km</Text>
                </View>
                  <View style={styles.modalViewLogo}>
                    <Button disabled={carPressed?.id == activeCar ? true : false} titleStyle={styles.modalActiveCarButton} title="Change to active" onPress={async () => {
                      if (!carPressed) {
                        return;
                      }
                      setNewActiveCar(carPressed?.id);
                      }}
                      containerStyle={{backgroundColor:"green"}}                   
                      type="clear"
                   
                   />
                    <Button titleStyle={{color:"red"}} title="Delete" onPress={async () => {
                      if (!carPressed) {
                        return;
                      }
                      deleteCar(carPressed?.id);
                      }}
                      type="clear"
                      />
                  </View>
                </View>
                  <View style={styles.modalInfoViewThirdRow}>
                    <View style={styles.modalInfoViewThirdRowInfoWithLogo}>
                    <Feather style={{textAlign:"center"}} name="info" size={30} color="green" />
                      <Text style={styles.modalText}>{"2018"}</Text>
                    </View>
                    <View style={styles.modalInfoViewThirdRowInfoWithLogo}>
                    <Feather style={{textAlign:"center"}} name="chevrons-up" size={30} color="green" />
                      <Text style={styles.modalText}>{"FWD"}</Text>
                    </View>
                    <View style={styles.modalInfoViewThirdRowInfoWithLogo}>
                      <MaterialIcons style={{textAlign:"center"}} name="local-gas-station" size={30} color="green" />
                      <Text style={styles.modalText}>{"97%"}</Text>
                    </View>
                  </View>
                  </View>
              <Text style={styles.modalContainerActiveText}>{carPressed?.id == activeCar ? "Active" : ""}</Text>
            </View>

          </View>
        </Modal>

        {/* BEFORE SUNDAY */}
        {/* CREATE A COMPONENT FROM THE ITEM */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(false);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>Add Car!</Text>
              <Input
                placeholder="registeration number"
                onChangeText={(text) => {
                  setRegistrationNumber(text);
                }}
              />
              <Input
                placeholder="fuel type"
                onChangeText={(text) => {
                  setCarType(text);
                }}
              />
               <Input
                placeholder={carType != null ? carType: "carType"}
                disabled={true}
              />
              <Button
                title="Select car type"
                titleStyle={{color:"green"}}
                buttonStyle={{borderColor:"green"}}
                type="outline"
                onPress={() => {
                  setBottomSheetVisible(true);
                }}
              />
              <View style={{marginBottom:"10%"}}></View>
              <View style={{width: "100%", flexDirection:"row", justifyContent: "space-around"}}>
              <Button
                titleStyle={{color:"green"}}
                title="Add"
                onPress={async () => {
                  newCar();
                }}
                type="clear"
              />
              <Button
                titleStyle={{color:"red"}}
                title="Cancel"
                onPress={async () => {
                  newCar();
                }}
                type="clear"
              />
              </View>
            </View>
          </View>
        </Modal>
        <BottomSheet isVisible={bottomSheetVisible}>
          {list.map((l, i) => (
            <ListItem
              key={i}
              containerStyle={l.containerStyle}
              onPress={() => {
                if(l.title != "Cancel") {
                  setCarType(l.title);
                }
                setBottomSheetVisible(false);
              }}
            >
              <ListItem.Content>
                <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          ))}
        </BottomSheet>
      </View>
    </>
  );
};

export default carView;