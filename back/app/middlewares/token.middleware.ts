import { Response } from './../models/response.interfaces';
import express from 'express';
import { decodeToken, tokenIsExpired, tokenIsReadable } from '../services/token.service';

export function checkToken(
  req: express.Request<any, any, any, any>,
  res: express.Response<any>,
  next: express.NextFunction
): express.Response<any> | void {
  const token = req.headers.authorization;

  // Check if token is readable
  if (!tokenIsReadable(token)) {
    return res.status(403).json({
      error: 'Authorization Token is malformed',
    } as Response.Error);
  }

  if (!token) {
    // Token wasn't sent
    return res.status(403).json({
      error: "Authorization Token wasn't provided",
    } as Response.Error);
  }

  try {
    if (tokenIsExpired(token))
      // Token is expired
      return res.status(403).json({
        error: 'Authorization Token is outdated',
      } as Response.Error);

    try {
      // Token could be verified by secret
      decodeToken(token);
      next();
    } catch (error) {
      return res.status(403).json({
        error: "Authorization Token couldn't be verified",
      } as Response.Error);
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      error: 'Something went wrong while checking token',
    } as Response.Error);
  }
}
