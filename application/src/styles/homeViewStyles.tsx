import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  home: {
    marginTop: "5%",
  },
  upperBar: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  helloMessageContainer: {
    alignItems: "flex-start",
    paddingLeft: "5%",
  },
  helloMessage: {
    fontSize: 35
  },
  config: {
    padding: "7%",
  },
  configFace: {
    padding: "7%",
    color: "#006400",
  },
  configSettings: {
    padding: "7%",
    color: "#006400",
  },
  dailyTip: {
    padding: 0,
  },
  card: {
    borderRadius: 10,
  },
  image: {
    alignItems: "center",
  },
  imageSlot: {
    maxHeight:200,
    maxWidth:300
  },
  progres: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingTop: "5%",
    paddingBottom: "5%",
  },
  progresSlot: {
    flex: 1,
    flexDirection: "column",
    alignContent: "center",
    alignItems:"center",
  },
  firstMeter: {
    fontSize: 30,
    textAlign: "center",
    width: "100%",
  },
  secondParam: {
    fontSize: 12,
    textAlign: "center",
  
    width: "100%",
  },
  bottomCardStyle1: {
    backgroundColor: "#006400",
    borderRadius: 10,
  },
  bottomCardStyle2: {
    backgroundColor: "white",
    borderRadius: 10,
    borderColor: "#006400",
    borderStyle: "dotted"
  },
  bottomCards: {
    flexDirection: "column",
    justifyContent: "center",
  },
  bottomCardDetail: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  bottomCardContent: {
    color: "white",
    fontSize: 20,
    width: "45%",
  },
});