import React from 'react'
import styled from 'styled-components'
import { Container } from 'react-bootstrap'
import forrest_couple from '../assets/forrest_couple.jpeg'

const Styles = styled.div`
    .background {
        background: url(${forrest_couple}) no-repeat fixed bottom;
        background-size: cover;
        color: #ccc;
        height: 300px;
        position: relative;
        z-index: -2;
    }
    .overlay {
        background-color: #000;
        opacity: 0.6;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: -1;
    }
`;

export const Home = () => (
    <Styles>
        <div fluid className="background">
            <div className="overlay">
                <Container>
                <h2>Lorem ipsum about</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                It has survived not only five centuries, but also the leap into electronic typesetting, 
                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus 
                PageMaker including versions of Lorem Ipsum.</p>
                </Container>
            </div>
        </div>
    </Styles>  
)
