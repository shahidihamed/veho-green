import * as React from "react";
import { View, Text, Switch, Image, Platform } from "react-native";
import { ListItem, Card, Avatar, Input, Button } from "react-native-elements";
import { styles } from "../styles/profileSettingsView";
import { Octicons } from '@expo/vector-icons';
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import apiClient from "../helpers/apiClient";
import { AuthContext } from "../context/context";
import { storeData , retrieveData, clearCache } from "../helpers/sessionStorage";

export interface User  {
  token: string;
  user: UserInfo;
}

export interface UserInfo  {
  cars: String[];
  events: any[];
  username: String;
  firstName: String;
  lastName: String;
  email: String;
  dateOfRegistration: Date;
  id: String;
  activeCar: String;
}

const profileSettingsView = () => {
  const [user, setUser] = React.useState<User | null>(null);
  const [started, setStarted] = React.useState<boolean>(false);
  const { signOut } = React.useContext(AuthContext);
  
  const getUser = () => {
    apiClient("/user", "GET", null,null, null, true).then( async (data: any) => {
      try {
        setUser(data)
      } catch(err) {
        alert(err);
      }
    }).catch((err) => {
      alert(err)
    })
  }

  React.useEffect((): void => {
    if(!started) {
      getUser();
      setStarted(true);
    }
  })
  return (
    <>
     <ScrollView>
      <Card containerStyle={styles.card}>
        <View style={styles.profileImage}>
        <Octicons name="person" size={120} style={styles.profileImageHolder}/>
        </View>
        <Card.Divider/>
        <Card.Title>{user?.user?.username}</Card.Title>
        <Card.Title>{user?.user?.firstName}</Card.Title>
        <Card.Title>{user?.user?.lastName}</Card.Title>
        <Card.Title>{user?.user?.email}</Card.Title>
        <Card.Title>{user?.user?.id}</Card.Title>
        <Card.Title>{user?.user?.dateOfRegistration}</Card.Title>
        <Card.Title>{user?.user?.cars.length}</Card.Title>
        <Card.Divider/>
        <Button buttonStyle={{borderColor:"red"}} titleStyle={{color:"red"}} type="outline" onPress={(async () => {await clearCache(); signOut(); })} title={"Logout"}/>
        <Card.Divider/>
      </Card>
      </ScrollView>
    </>
  );
};
export default profileSettingsView;
