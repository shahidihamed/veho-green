import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  card: {
    borderRadius: 10
  },
  avatarHeader: {
    textAlign: "center",
    alignItems: "center",
  },
  profileImage: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: "5%",

  },
  profileImageHolder: {
    color: "#006400"
  }
});
