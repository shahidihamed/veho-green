import * as React from "react";
import { Dimensions, SafeAreaView, ScrollView, View } from "react-native";
import { Card } from "react-native-elements";
import { devColors, styles } from "../styles/statisticsViewStyles";
import { LineChart } from "react-native-chart-kit";
import { TouchableOpacity } from "react-native-gesture-handler";
import { createStackNavigator } from "@react-navigation/stack";
import { ActivitiesViewNavigator } from "./activitiesView";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;
const chartWidth = windowWidth - 80;
const chartHeight = windowHeight / 7;

const chartConfig = {
  backgroundGradientFromOpacity: 0,
  backgroundGradientToOpacity: 0,
  color: (opacity = 1) => `rgba(12, 118, 66, ${opacity})`,
  strokeWidth: 3,
  barPercentage: 0.8,
  fillShadowGradient: devColors.green,
  fillShadowGradientOpacity: 1,
  decimalPlaces: 0,
};

// Return random number for eco-point as long as there is no eco-point
// in fetched events
const returnRandom = (): number[] => {
  let arr: number[] = [];
  for (let i = 0; i < 7; i++) {
    arr.push(Math.floor(Math.random() * 99));
  }
  return arr;
};

const returnDataSet = (): any => {
  return {
    datasets: [
      {
        labels: ["Ma", "Ti", "Ke", "To", "Pe", "La", "Su"],
        data: returnRandom(),
        color: (opacity = 1) => `rgba(0, 230, 64, 1)`,
        strokeWidth: 1,
      },
    ],
  };
};

// Displays Activities chart
const activitiesChart = () => {
  return (
    <LineChart
      data={returnDataSet()}
      width={chartWidth}
      height={chartHeight}
      chartConfig={chartConfig}
      bezier
      segments={2}
      withInnerLines={true}
      withOuterLines={false}
      withHorizontalLabels={true}
      style={styles.chart}
      fromZero={true}
      withHorizontalLines={true}
      withVerticalLines={false}
    />
  );
};

// Displays Purchases chart
// This chart is implemented only to display the concept of
// a possible future feature.
const purchasesChart = () => {
  return (
    <LineChart
      data={returnDataSet()}
      width={chartWidth}
      height={chartHeight}
      chartConfig={chartConfig}
      bezier
      segments={2}
      withInnerLines={true}
      withOuterLines={false}
      withHorizontalLabels={true}
      style={styles.chart}
      fromZero={true}
      withHorizontalLines={true}
      withVerticalLines={false}
    />
  );
};

// Displays Household chart
// This chart is implemented only to display the concept of
// a possible future feature.
const householdChart = () => {
  return (
    <LineChart
      data={returnDataSet()}
      width={chartWidth}
      height={chartHeight}
      chartConfig={chartConfig}
      bezier
      segments={2}
      withInnerLines={true}
      withOuterLines={false}
      withHorizontalLabels={true}
      style={styles.chart}
      fromZero={true}
      withHorizontalLines={true}
      withVerticalLines={false}
    />
  );
};

// Navigator for page stack
const StatStack = createStackNavigator();
export const StatisticViewNavigator = () => {
  return (
    <StatStack.Navigator headerMode={"none"}>
      <StatStack.Screen name="Statistics" component={statisticsView} />
      <StatStack.Screen name="Activities" component={ActivitiesViewNavigator} />
      {/*TODO: in future purchasesView, householdView*/}
    </StatStack.Navigator>
  );
};

// Main view
const statisticsView = ({ navigation }: any) => {
  return (
    <SafeAreaView style={[styles.main, styles.droidSafeArea]}>
      <ScrollView style={{ flex: 1, marginTop: 16 }}>
        <View nativeID="top-card-container" style={styles.cardContainer}>
          <TouchableOpacity
            style={styles.card}
            onPress={() => navigation.navigate("Activities")}
          >
            <Card containerStyle={styles.card}>
              <Card.Title style={styles.cardTitle}>Activities</Card.Title>
              <Card.Divider />
              {activitiesChart()}
            </Card>
          </TouchableOpacity>
        </View>
        <View nativeID="middle-card-container" style={styles.cardContainer}>
          <TouchableOpacity style={styles.card}>
            <Card containerStyle={styles.card}>
              <Card.Title style={styles.cardTitle}>Purchases</Card.Title>
              <Card.Divider />
              {purchasesChart()}
            </Card>
          </TouchableOpacity>
        </View>
        <View nativeID="bottom-card-container" style={styles.cardContainer}>
          <TouchableOpacity style={styles.card}>
            <Card containerStyle={styles.card}>
              <Card.Title style={styles.cardTitle}>Household</Card.Title>
              <Card.Divider />
              {householdChart()}
            </Card>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default statisticsView;
