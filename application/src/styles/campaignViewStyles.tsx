import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
  },
  ecoPoints: {
    justifyContent: "center",
    alignItems: "center",
    height: "23%",
    backgroundColor: "#006400",
    borderBottomRightRadius: 45,
  },
  points: {
    fontSize: 35,
    color: "white",
    
  },
  list: {
    paddingLeft: "5%",
  },
  listContent: {
    justifyContent: "space-around"
  },
  arrowRigth: {
    
  },
  coupon: {
    width: "90%",
    marginBottom: "2%",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    width: "80%",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  }
});
