import * as React from "react";
import {
  FontAwesome,
  MaterialCommunityIcons,
  AntDesign,
} from "@expo/vector-icons";
import { Octicons } from "@expo/vector-icons";
import { styles } from "../styles/homeViewStyles";
import {
  StyleSheet,
  View,
  Text,
  Button,
  Dimensions,
  Image,
} from "react-native";
import { Card, ListItem, Icon } from "react-native-elements";
import tipsView from "./tipsView";
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
} from "react-native-gesture-handler";
import { color } from "react-native-reanimated";
const dimensions = Dimensions.get("window").width;

const householdView = () => {
  const [meter1, setMeter1] = React.useState(0);
  const [meter2, setMeter2] = React.useState(0);
  const [metersSet, setMeterBool] = React.useState(false);

  const setRand = (): void => {
    setMeter1(Math.floor(Math.random() * 10));
    setMeter2(Math.floor(Math.random() * 10));
  };

  React.useEffect((): void => {
    if (!metersSet) {
      setRand();
      setMeterBool(true);
    }
  });

  return (
    <View>
      <Text>Activities view</Text>
    </View>
  );
};

export default householdView;

