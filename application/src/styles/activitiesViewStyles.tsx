import { StyleSheet, Platform, Dimensions } from "react-native";

export const devColors = {
  black: "#000",
  blue: "dodgerblue",
  green: "#006400",
  orange: "orange",
  pink: "pink",
  red: "red",
  white: "#fff",
  yellow: "yellow",
  darkGreen: "#0C7644",
};

export const styles = StyleSheet.create({
  card: {
    borderRadius: 30,
    backgroundColor: devColors.white,
  },
  cardContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  cardTitle: {
    textAlign: "left",
  },
  chart: {
    backgroundColor: devColors.white,
  },
  container: {
    marginBottom: 15,
    justifyContent: "center",
    flex: 1,
  },
  ecoEvent: {
    margin: 4,
    marginHorizontal: 10,
    paddingVertical: 20,
    fontSize: 22,
    height: 30,
    borderRadius: 30,
    flexDirection: "row",
    flex: 1,
  },
  eventList: {},
  itemIconView: {
    justifyContent: "center",
    marginLeft: 30,
    flex: 1,
  },
  itemDate: {
    justifyContent: "center",
    marginLeft: 10,
    flex: 1,
  },
  itemType: {
    justifyContent: "center",
    flex: 1,
  },
  itemPoints: {
    alignItems: "flex-start",
    fontWeight: "bold",
    color: devColors.green,
    flex: 1,
  },
  main: {
    paddingBottom: 20,
  },
  mapStyle: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  monthBar: {
    flexGrow: 1,
    height: 80,
    backgroundColor: devColors.white,
    marginBottom: 20,
    marginTop: 20,
  },
  monthSeparator: {
    height: 50,
    marginTop: 10,
    width: "100%",
    justifyContent: "center",
  },
  monthSeparatorText: {
    flex: 1,
    fontSize: 20,
    fontWeight: "normal",
    textAlign: "left",
    paddingLeft: 20,
    backgroundColor: devColors.white,
  },
  monthTitle: {
    width: 70,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: devColors.white,
  },
  monthTitleText: {
    fontSize: 18,
    justifyContent: "center",
    alignItems: "center",
  },
  monthTitleTouchable: {
    width: 80,
    padding: 20,
  },
  droidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? 2 : 0,
  },
});
