import React, { useState } from "react";
import { NavigationContainer, RouteProp } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { Ionicons } from "@expo/vector-icons";
import RouteView from "./src/views/routeView";
import CarView from "./src/views/carView";
import homeViewNavigator from "./src/navigator/homeViewNavigator";
import { StatisticViewNavigator } from "./src/views/statisticView";
import LoginView from "./src/views/loginView";
import RegisterView from "./src/views/registerView";
import { AuthContext } from "./src/context/context";

export default function App({ navigation }) {
  const [isSignedIn, setIsSignedIn] = useState<boolean>(false);
  const Stack = createStackNavigator();
  const Tab = createBottomTabNavigator();
  const authContext = React.useMemo((): any => {
    return {
      signIn: async (data) => {
        setIsSignedIn(true);
      },
      signUp: () => {
        setIsSignedIn(true);
      },
      signOut: () => {
        setIsSignedIn(false);
      },
      render: () => {
      }
    };
  }, []);
 
  return (
    <AuthContext.Provider value={authContext}>
    <NavigationContainer>
      {isSignedIn ? (
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              return returnIcon(route, focused, color, size);
            },
          })}
          tabBarOptions={{
            activeTintColor: "green",
            inactiveTintColor: "gray",
          }}
        >
          <Tab.Screen name="Home" component={homeViewNavigator} />
          <Tab.Screen name="Statistics" component={StatisticViewNavigator} />
          <Tab.Screen name="Route planner" component={RouteView} />
          <Tab.Screen name="Car" component={CarView} />
        </Tab.Navigator>
      ) : (
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            options={{ headerShown: false }}
            component={LoginView}
          />
          <Stack.Screen
            name="Register"
            options={{ headerShown: false }}
            component={RegisterView}
          />
        </Stack.Navigator>
      )}
    </NavigationContainer>
    </AuthContext.Provider>
  );
}

const returnIcon = (
  route: RouteProp<Record<string, object | undefined>, string>,
  focused: boolean,
  color: string,
  size: number
) => {
  {
    let iconName: string = "";
    if (route.name === "Route planner") {
      iconName = "ios-map";
    } else if (route.name === "Statistics") {
      iconName = "md-stats";
    } else if (route.name === "Home") {
      iconName = "md-home";
    } else if (route.name === "Car") {
      iconName = "md-car";
    } else if (route.name === "Login") {
      iconName = "md-log-in";
    }
    return <Ionicons name={iconName} size={size} color={color} />;
  }
};
