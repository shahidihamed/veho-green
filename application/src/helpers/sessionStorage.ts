import { AsyncStorage } from "react-native";

/**
 * 
 * Deprecated  
 * Helper function to store data in AsyncStorage
 */
export const storeData: Function = (key: string, value: string): Promise<void> => {
  return AsyncStorage.setItem(key, value);
};

export const retrieveData: Function = async (key: string): Promise<string | null> => {
  return AsyncStorage.getItem(key);
};

export const clearCache: Function = () => {
  return AsyncStorage.clear();
}
