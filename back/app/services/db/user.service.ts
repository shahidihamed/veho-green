import { User } from '../../models/db/user.interfaces';
import { Users } from '../../db/users.schema';
import { Car } from '../../models/db/car.interfaces';
import { EventsModel } from '../../models/db/event.interfaces';

export async function create(user: User): Promise<User> {
  try {
    return (await Users.create(user)).toJSON();
  } catch (error) {
    console.error('Error happened in userCreate function');
    throw error;
  }
}

export async function getById(id: string): Promise<User | undefined> {
  try {
    return (await Users.find({ _id: id }))[0]?.toJSON();
  } catch (error) {
    console.error('Error happened in getById function');
    throw error;
  }
}

export async function findUserByUsernameOrEmail(username?: string, email?: string): Promise<User | undefined> {
  try {
    if (username) {
      return (await Users.find({ username }))[0]?.toJSON();
    } else if (email) {
      return (await Users.find({ email }))[0]?.toJSON();
    } else {
      return undefined;
    }
  } catch (error) {
    console.error('Error happened in findUserByUsernameOrEmail function');
    throw error;
  }
}

export async function updateUserPoints(userId: string): Promise<void> {
  try {
    const user: User = (await Users.find({ _id: userId }).populate('events'))[0]?.toJSON();

    user.events.forEach((event) => {
      user.points += event.points;
    });
  } catch (error) {
    console.error('Error happened in findUserByUsernameOrEmail function');
    throw error;
  }
}

export async function updateUser(userId: string, fieldsToUpdate: any): Promise<User | undefined> {
  try {
    return (await Users.updateOne({ _id: userId }, fieldsToUpdate))[0]?.toJSON();
  } catch (error) {
    console.error('Error happened in findUserByUsernameOrEmail function');
    throw error;
  }
}

export async function getUsersCars(userId: string): Promise<Car[] | undefined> {
  try {
    return (await Users.find({ _id: userId }).populate('cars'))[0]?.toJSON().cars.map((car) => car);
  } catch (error) {
    console.log(error);
    console.error('Error happened in getUsersCars function');
    throw error;
  }
}

export async function getUsersEvents(
  userId: string
): Promise<
  EventsModel.Car[] | EventsModel.PublicTransport[] | EventsModel.Shopping[] | EventsModel.Walk[] | undefined
> {
  try {
    return (
      await Users.find({ _id: userId }).populate({
        path: 'events',
        populate: {
          path: 'checkpoints',
        },
      })
    )[0]
      ?.toJSON()
      .events.map((event) => event.toJSON());
  } catch (error) {
    console.error('Error happened in getUsersEvents function');
    throw error;
  }
}
