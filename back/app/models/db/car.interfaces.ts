import { User } from './user.interfaces';
import { DataBaseObject } from './general.interfaces';

export interface Car extends DataBaseObject {
  users: string[] | User[];
  fuelType: CarFuelType;
  carType: CarType;
  registrationNumber: string;
}

export enum CarFuelType {
  'benzine',
  'diesel',
}

export enum CarType {
  'a-series',
  'b-series',
  'c-series',
  's-series',
}
