# Front-end

![Logo](./assets/logo.png "Logo")


## About

    This documentation is for our School project that was made in partnership with VEHO. Idea of this application was to gather data from variety os sources and show them to the user. Application is now centered in around driving emsissions

## Getting started

Programs that you need to start this application
-   node
-   npm
-   expo

#### Starting the application
1. Change directory to /application/
2. $ npm i
3. $ npm start

        Run your own backend application:
1.  Open new terminal
2.  $ cd {projectFolder}/back/
3.  $ npm run dev

## Features

* route planner
* active car change
* detailed statistics about recent trip
* Tips to lower emissions
* Spend your eco points 


## Video

[Video about the application](https://drive.google.com/file/d/1YXiNYW9B3nlcYOn6SYHQLNxKZi80lx0y/view)

## Known bugs

*   in Campaign redeeming the ticket on the bottom. it deletes the both tickets( something to do with slice... )





 