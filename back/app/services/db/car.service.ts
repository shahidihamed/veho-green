import { Car, CarFuelType, CarType } from '../../models/db/car.interfaces';
import { Cars } from '../../db/cars.schema';
import { Users } from '../../db/users.schema';

export async function create(car: Car, userId: string): Promise<Car> {
  try {
    const newCar: Car = (await Cars.create(car)).toJSON();
    await Users.updateOne({ _id: userId }, { $push: { cars: newCar.id } });
    return newCar;
  } catch (error) {
    console.error('Error happened in createCar function');
    throw error;
  }
}

export async function getById(id: string): Promise<Car | undefined> {
  try {
    return (await Cars.find({ _id: id }))[0]?.toJSON();
  } catch (error) {
    console.log(error);
    console.error('Error happened in getById function');
    throw error;
  }
}

export async function getByRegistrationNumber(registrationNumber: string): Promise<Car | undefined> {
  try {
    return (await Cars.find({ registrationNumber: registrationNumber }))[0]?.toJSON();
  } catch (error) {
    console.error('Error happened in getByUserId function');
    throw error;
  }
}

export async function update(
  id: string,
  fieldsToUpdate: { fuelType?: CarFuelType; carType?: CarType; registrationNumber?: string }
): Promise<Car | undefined> {
  try {
    await Cars.updateOne({ _id: id }, fieldsToUpdate);
    return (await Cars.find({ _id: id }))[0]?.toJSON();
  } catch (error) {
    console.log(error);
    console.error('Error happened in update function');
    throw error;
  }
}

export async function remove(carId: string): Promise<void> {
  try {
    const car: Car = await getById(carId);

    for (const userId of car.users) {
      await Users.updateOne({ _id: userId }, { $pull: { cars: carId } });
    }

    await Cars.deleteOne({ _id: carId });
  } catch (error) {
    console.log(error);
    console.error('Error happened in getById function');
    throw error;
  }
}
