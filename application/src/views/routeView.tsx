import React, { useState } from "react";
import { View, Text, StyleSheet, Animated, ScrollView, Dimensions } from "react-native";
import { Slider, Input, Image, Button, Card } from "react-native-elements";
import { styles } from "../styles/routeViewStyles";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import MapView, { AnimatedRegion, LatLng, MapEvent, Marker, MarkerProps, Polyline } from 'react-native-maps';
import * as Location from 'expo-location';
import apiClient from "../helpers/apiClient";
import { storeData , retrieveData } from "../helpers/sessionStorage";
import { Car } from "../models/CarModel";

/**
 * 
 * View for route planner. 
 */

const routePlannerView = ({ navigation }: any) => {
  const [consumption, setConsumption] = useState<number>(0.0);
  const [distance, setDistance] = useState<number>(0.0);
  const [cost, setCost] = useState<number>(0.0);
  const [car, setCar] = React.useState<Car | null>(null);
  const [locationInfo, setLocation] = React.useState<Location.LocationObject>();
  const [markers, setMarker] = React.useState<MarkerProps>();
  const [userMarker, setUserMarker] = React.useState<MarkerProps>();

  const calclulateDistance: Function = (pointALatLng, pointBLatLng) => {

    //Haversine formula
    //https://en.wikipedia.org/wiki/Haversine_formula
    const R = 6371; // km
    const dLat = toRad(pointBLatLng.latitude-pointALatLng.latitude);
    const dLon = toRad(pointBLatLng.longitude-pointALatLng.longitude);

    const a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(toRad(pointALatLng.latitude)) * Math.cos(toRad(pointBLatLng.latitude)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    const d = R * c;
    return d;
  }
  const toRad: Function = (Value) =>  {
      return Value * Math.PI / 180;
  }

  const calculateConsumption: Function = () => {
      if(markers && userMarker) {
        const distance = calclulateDistance(markers.coordinate, userMarker.coordinate)
        setDistance(distance);
        const cons = (distance / 6.70);
        setConsumption(Math.floor(cons));
        setCost(Math.floor(cons * 1.5));  
      }
    };

  const checkActiveCar: Function = async () => {
    try {
      const token = await retrieveData("token"); 
      if(!token) {
        alert("Error fetching token!")
      }
      let userInfo: any = await apiClient("/user", "GET", null,null, null, true);
      if(!userInfo.user.activeCar) {
        setCar(null);
      } else {
        let carDetail: any = await apiClient("/car/" + userInfo.user.activeCar, "GET", null,null, null, true);
        setCar(carDetail.car);
      }
    } catch(e: any) {
      console.log(e);
    }
  }

  const addMarker: Function = async (coordinates: LatLng): Promise<any> => {
    const marker: MarkerProps = {
      coordinate: coordinates,
      description:"Destination",
      title: "To:"
    }
    setMarker(marker);
  }

  const checkPermission: Function = async (): Promise<any> => {
    let { status } = await Location.requestPermissionsAsync();
      if (status !== 'granted') {
        alert('Permission to access location was denied');
        return false
      }
      return true
  }
  React.useEffect(() => {
    checkPermission();
  }, []);


  React.useEffect(() => {
    
    navigation.addListener('focus', async () => {
      checkActiveCar();
      let location: Location.LocationObject = await Location.getCurrentPositionAsync({});
      setLocation(location);
      if(location) {
        const coordinates: LatLng = {
          latitude: location.coords.latitude,
          longitude: location.coords.longitude
        } 
        const userMarker: MarkerProps = {
          coordinate: coordinates,
          description:"Users location",
          title: "User"
        } 
        setUserMarker(userMarker);
      }
    });
  }, []);

  return (
    <ScrollView>
      <View style={styles.ecoPoints}>
        <Text style={styles.points}>Route planner</Text>
        <MaterialCommunityIcons name="map-marker-path" size={90} color="white" />
      </View>
      <View style={styles.content}>
        <Card>
        <Text style={styles.smallHeader}>Active Car:</Text>
          <View
            style={{ flex: 1, alignItems: "stretch", justifyContent: "center", borderRadius: 15 }}>
            <Card.Title>{car?.registrationNumber}</Card.Title>
            <Card.Divider></Card.Divider>
            <Card.Title>{car?.carType}</Card.Title>
          </View>
        </Card>
        <Card>        
        <View style={styles1.container}>
        { locationInfo ? 
        <MapView onPress={(data: MapEvent) => {addMarker(data.nativeEvent.coordinate); calculateConsumption()}} style={styles1.mapStyle} initialRegion={{latitude:locationInfo.coords.latitude,longitude:locationInfo.coords.longitude, latitudeDelta: locationInfo.coords.latitude, longitudeDelta:locationInfo.coords.longitude}}>
        { userMarker ? <Marker
          coordinate={userMarker?.coordinate}
          title={userMarker?.title}
          description={userMarker?.description}
        /> : null }
        { markers ? <Marker
          coordinate={markers?.coordinate}
          title={markers?.title}
          description={markers?.description}
        /> : null }
        {
          userMarker && markers ? <Polyline coordinates={[userMarker.coordinate as LatLng , markers.coordinate as LatLng]}/> : null
        } 
      </MapView>
           : <Text>No location provided!</Text>}
       {/* {location ? <Marker coordinate={{latitude: location.latitude, longitude:location.longitude}}/> :}*/}
      </View>
      </Card>
      </View>
      <View style={styles.calculatedData}>
          <Card containerStyle={{ height: "80%", width: "28%" , borderRadius: 15, borderColor: "#006400"}}>
            <Card.Title>{"Fuel"}</Card.Title>
            <Card.Divider></Card.Divider>
            <Card.Title>{consumption} L.</Card.Title>
          </Card>
          <Card containerStyle={{ height: "80%", width: "28%" , borderRadius: 15, borderColor: "#006400" }}>
            <Card.Title>{"Distance"}</Card.Title>
            <Card.Divider></Card.Divider>
            <Card.Title>{Math.floor(distance)} Km.</Card.Title>
          </Card>
          <Card containerStyle={{ height: "80%", width: "28%" , borderRadius: 15, borderColor: "#006400" }}>
            <Card.Title>{"cost"}</Card.Title>
            <Card.Divider></Card.Divider>
            <Card.Title>{cost} €</Card.Title>
          </Card>
      </View>
      </ScrollView>
  );
};

const styles1 = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: "100%",
    height: Dimensions.get('window').height - 400,
  },
});

export default routePlannerView;
