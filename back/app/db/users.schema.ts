import mongoose, { Schema } from 'mongoose';

const userSchema: Schema<any> = new mongoose.Schema({
  username: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true },
  passwordHash: { type: String, required: true },
  dateOfRegistration: { type: Date, required: true },
  points: { type: Number, required: true },
  activeCar: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Car',
  },
  cars: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Car',
    },
  ],
  events: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Event',
    },
  ],
});

userSchema.set('toJSON', {
  transform: (document, ret) => {
    ret.id = ret._id.toString();
    delete ret._id;
    delete ret.__v;
  },
});

export const Users = mongoose.model('User', userSchema);
