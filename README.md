## Table of content
- [Project info](#markdown-header-project-information)
- [What is VehoGreen?](#markdown-header-what-is-vehogreen)
- [Target user groups](#markdown-header-target-user-groups)
- [Features](#markdown-header-features)
- [Application demo](#markdown-header-application-demo)
	- [Registration flow](#markdown-header-registration-flow)
	- [Sign-in & Sign-out](#markdown-header-sign-in-and-sign-out)
	- [User status & Partner deals](#markdown-header-user-status-and-partner-deals)
	- [Statistics and History](#markdown-header-statistics-and-history)
	- [Activity details](#markdown-header-activity-details)
	- [Route planner](#markdown-header-route-planner)
	- [Managing cars](#markdown-header-managing-cars)
- [Used technologies](#markdown-header-used-technologies)
- [Getting started](#markdown-header-getting-started)
	- [Foundation](#markdown-header-foundation)
	- [Installation](#markdown-header-installation)
	- [Running application](#markdown-header-running-application)
- [Technical info](#markdown-header-technical-info)
- [Presentation material](#markdown-header-presentation-material)
- [The team behind VehoGreen](#markdown-header-the-team-behind-vehogreen)

![alt text][logo]


# VehoGreen - Building a better world!

-----

## Project information:  

Mobile application - Bonus system for eco-friendly carbon footprint


| Course | Organizer | Partner |
| ------ | --------- | ------- |
| ADP Innovation Project 2020 | [**Helsinki Metropolia University of Applied Sciences School of ICT**](https://www.metropolia.fi/en) | [**Veho Oy Ab**](https://www.veho.fi/en/) |

-----


## What is VehoGreen?

- VehoGreen is a mobile application for daily use.
- Purpose of VehoGreen project is to motivate an individual to reduce own carbon footprint (mainly from driving style).
- Tracks personal activity and carbon emissions.
- Provides easy to use and understandable statistics.
- Offers personal bonuses / benefits.

-----

## Target user groups:

- Specifically people who drive a car.
- Generally people intrested in eco-friendly living.
- Generally people interested in individual benefits.

-----

## Features

- ### Track driving style
	With this feature you track your own driving style and make necessary changes to improve your economical driving style.  
	##  
- ### History & Statistics
	You can see efficient data from your trips, daily driving, work routes and etc. This helps you to see more efficients ways to plan every day routes and travels.  
	##  
- ### Route planner
	Gives you alternative choices when you are planning your trips. It also works as a navigator with traffic and accident notifications. This way you can avoid these time consuming events with your trips.  
	##  
- ### Eco-points
	While you are driving eco-friendly you accumulate ECO-points. More friendly you drive more points you get.  
	##  
- ### Partner deals
	Deals from our partners that save you MONEY! You get discounts, great deals and you can even use your ECO-points to get free bonuses.  
	##  
- ### Tips & Advices
	Don’t know how to start driving ECO-friendly? Don’t worry. Application is programmed to provide you tips and alternative choices to help you to reduce your carbon footprint.  
	##  

-----

## Application demo

### Registration flow  
##
- Enter email address
- Enter password
- Enter username
- Enter first name
- Enter last name
- Register  
##  

![alt text][registration]  

-----

### Sign-in and Sign-out
##  

#### Signing-in 
- Enter username
- Enter password
- Sign-in
#### Signing-out
- Go to profile
- Press logout  
##  

![alt text][sign-in]  

-----

### User Status and Partner deals
##  

#### User status in main page
- Go to main page
- Shows current trips
- Shows current steps
- Shows total daily progress
- Shows total weekly progress
- Additionally shows tips for reducing daily emissions
#### Partner Deals
- Go to 'Find Campaigns' in main page
- Shows total earned eco-points
- shows current deals/offers
- Click on deal for info or redeem  
##  

![alt text][deals]

-----

### Statistics and History
##  

#### Statistics
- Go to 'statistics' tab
- Activity chart shows eco-points progress gained by activities over passed week
- Purchases chart shows eco-points progress gained by purchases over passed week (concept)
- Household chart shows eco-points progress gained by household over passed week (concept)
#### History
- Click on each chart to select history category
- Select month from month bar to view history of the month  
##  

![alt text][statistics]

-----

### Activity details
##  

- Select month to view list of events of the month
- Select event to view details
- Shows type of event
- Shows gained eco-points
- Shows trip distance
- Shows location on interactive map  
##  

![alt text][event-info]

-----

### Route Planner
##  

- Go to 'Route planner' tab
- Select destination on interactive map
- Route planner calculates estimations for the trip
- Shows Distance
- Shows fuel consumption estimation
- Shows trip cost estimation  
##  

![alt text][route-planner]

-----

### Managing Cars
##  

#### Add new car
- Go to 'Car' tab
- Press add car
- Enter registery number
- Enter fuel type
- Select car type
- Press 'Add car'
#### Remove car
- Go to 'Car' tab
- Select car from the list
- Press 'Delete'
#### Change active car
- Go to 'Car' tab
- Select car from the list
- Press 'Change to active'  
##  

![alt text][cars]

-----

## Used technologies:

#### Front-end

 - React Native
 - Expo
 - TypeScript
 
#### Back-end

 - Node.Js
 - MongoBD
 - TypeScript
 
-----

## Getting started
##  

### Foundation:  
###  

 - Install [_Git_](https://git-scm.com/)
 - Install [_Node.Js_](https://nodejs.org/en/)  
 - Install [_Expo cli_](https://docs.expo.io/workflow/expo-cli/)
 - (optional) Install Expo [_android_](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www) or [_IOS_](https://apps.apple.com/app/apple-store/id982107779) client

-----

### Installation:  
###  

 1. Clone this repository.  
 	
	HTTPS:  
	 `git clone https://shahidihamed@bitbucket.org/shahidihamed/veho-green.git`  
	 
 	or SSH:  
	 `git clone git@bitbucket.org:shahidihamed/veho-green.git`  
	##  
	  
 2. Change directory to  `<local path>/veho-green/application/`  
 
	##  
	
 3. Install dependencies by executing  `npm install`  
 
	##  

-----

### Running application:  
###  

 1. Change directory to  `<local path>/veho-green/application/`  
 
 	##
	
 2. Execute `npm start`  
 
 	##  
	
 3. Follow the presented instructions for opening mobile application on an emulator or actual mobile device
 	
	##  
	
-----
	
#### (optional) Running own backe-end application:  
 1. Open new terminal  
 	
	##
	
 2. Change directory to  `<local path>/veho-green/back/`  
 	
	##
	
 3. Execute `npm run dev`  
 	
	##
	
-----  

## Technical info  

See [_Back-end documentation_](https://bitbucket.org/shahidihamed/veho-green/src/master/back/README.md) for more technical information such as:  

- Server configuration
- API
- Database sctucture  

	##  
	

See [_Architectural diagram_](https://bitbucket.org/shahidihamed/veho-green/src/master/app-diagram.svg) of VehoGreen mobile application for information such as:

- Application architecture
- Connection model of back/front -end
- Data flow diagram
- Database diagram

-----

## Presentation material

You can download Veho-Green project presentation slides in following formats:

 - [Powerpoint slides](https://bitbucket.org/shahidihamed/veho-green/raw/e68428beebd6dde542593a95f803d1a752511438/demo/veho-green%20_%20project%20presentation.pptx)
 
 - [Pdf slides](https://bitbucket.org/shahidihamed/veho-green/raw/e68428beebd6dde542593a95f803d1a752511438/demo/veho-green%20_%20project%20presentation.pdf)

![alt_text][slides]

-----

## The team behind VehoGreen

| Name | Role | Contact |
|---|---|---|
|Jukka Juntunen|Front-end developer|[See Jukka's profile on Linkedin](http://www.linkedin.com/in/jukka-juntunen)|
|Jussi Nissinaho|Business & communications manager|[See Jussi's profile on Linkedin](http://www.linkedin.com/in/jussinissinaho)|
|Emil Toivainen|Mobile developer|[See Emil's profile on Linkedin](https://www.linkedin.com/in/emil-toivainen-b72105159)|
|Edvard Shalaev|Back-end developer|[See Edvard's profile on Linkedin](https://www.linkedin.com/in/edvardshalaev)|
|Hamed Shahidi|Mobile developer|[See Hamed's profile on Linkedin](http://www.linkedin.com/in/hamed-shahidi)|

![alt_text][team]








[logo]: https://i.postimg.cc/vHxnz4Hh/logo-m2.png "VehoGreen logo"
[registration]: https://bitbucket.org/shahidihamed/veho-green/raw/dff965fd6bfa88077aa2740b96581cccc9afc289/demo/registration.gif "Registration flow"
[sign-in]: https://bitbucket.org/shahidihamed/veho-green/raw/dff965fd6bfa88077aa2740b96581cccc9afc289/demo/signin.gif "Sign-in flow"
[deals]: https://bitbucket.org/shahidihamed/veho-green/raw/dff965fd6bfa88077aa2740b96581cccc9afc289/demo/deals.gif "Main pahe & Deals"
[statistics]: https://bitbucket.org/shahidihamed/veho-green/raw/dff965fd6bfa88077aa2740b96581cccc9afc289/demo/history.gif "Statistics & Monthly history"
[event-info]: https://bitbucket.org/shahidihamed/veho-green/raw/dff965fd6bfa88077aa2740b96581cccc9afc289/demo/statistics_and%20_history.gif "Activity details"
[route-planner]: https://bitbucket.org/shahidihamed/veho-green/raw/dff965fd6bfa88077aa2740b96581cccc9afc289/demo/route_planner.gif "Route planner"
[cars]: https://bitbucket.org/shahidihamed/veho-green/raw/dff965fd6bfa88077aa2740b96581cccc9afc289/demo/manage_cars.gif "Managing cars"
[slides]: https://bitbucket.org/shahidihamed/veho-green/raw/4fd3470a910c493d753022718c9af3a140b12fba/demo/veho-green_presentation_slides.gif "Presentation slides"
[team]: https://bitbucket.org/shahidihamed/veho-green/raw/ac03c6fc873442a620d4ac21ceed31c392c78793/demo/team.png "The Team behind Veho-Green project"

