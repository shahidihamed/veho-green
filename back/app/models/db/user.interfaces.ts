import { EventsModel } from './event.interfaces';
import { Car } from './car.interfaces';
import { DataBaseObject } from './general.interfaces';
export interface User extends DataBaseObject {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  points: number;
  passwordHash?: string;
  dateOfRegistration: Date | string;
  activeCar: string | Car;
  cars: string[] | Car[];
  events: string[] | EventsModel.Car[] | EventsModel.PublicTransport[] | EventsModel.Shopping[] | EventsModel.Walk[];
}
