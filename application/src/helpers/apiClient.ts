import { storeData , retrieveData } from "../helpers/sessionStorage";

const createUrl = (url: string, endpoint: string): string => {
  let fullPath = url += endpoint;
  return fullPath.replace("//", "/");;
}

/**
 * 
 * @param endpoint 
 * @param method 
 * @param query 
 * @param params 
 * @param body 
 * @param tokenNeeded 
 * 
 * @description "Helper function to communicate with the backend"
 */

const sendData = async (
  endpoint: string,
  method: string,
  query: string | null,
  params: string | null,
  body: any,
  tokenNeeded: boolean
): Promise<any> => {

  //GET FROM STORAGE?? or from .env file
  const url = "http://80.220.192.228:3039"
  let token: string | null ="";

  if(tokenNeeded) {
    try {
      token = await retrieveData("token");
      if(!token || token === null) {
        return Promise.reject("no token");
      }
    } catch (err) {
      return Promise.reject(err);
    }

  }
  
  let fullPath = createUrl(url, endpoint);
  
  if(params) {
    fullPath += "/" +params
  }
  return new Promise((resolve, reject) => {
    return fetch(fullPath, {
      method: method,
      headers: {
        Authorization: "" + token,
        Accept: "application/json",
        "Content-type": "application/json",
      },
      body
    })
      .then((response) => response.json())
      .then((res: any) => {
        return resolve(res);
      })
      .catch((err: any) => {
        return reject(err);
      });
  });
};


export default sendData;