import { StyleSheet, Platform, View, Dimensions } from "react-native";
import { colors } from "react-native-elements";

export const devColors = {
  black: "#000",
  blue: "dodgerblue",
  green: "#006400",
  orange: "orange",
  pink: "pink",
  red: "red",
  white: "#fff",
  yellow: "yellow",
  darkGreen: "#0C7644",
};

export const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    // overflow: "visible",
  },
  cardContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  cardTitle: {
    textAlign: "left",
    marginLeft: 20,
  },
  chart: {
    backgroundColor: devColors.white,
  },
  ecoEvent: {
    backgroundColor: devColors.white,
    margin: 10,
    marginHorizontal: 20,
    padding: 30,
    fontSize: 22,
    height: 30,
    borderRadius: 30,
    shadowColor: devColors.black,
    shadowOpacity: 1,
    shadowOffset: {
      width: 20,
      height: -20,
    },
    elevation: 3,
    justifyContent: "center",
    // flexDirection: "row",
    // backgroundColor: "#0000",
  },
  eventList: {
    // backgroundColor: devColors.orange,
  },
  main: {
    // backgroundColor: devColors.blue,
    paddingBottom: 20,
    // marginTop: 20,
    // display: "flex",
  },
  monthBar: {
    height: 60,
    backgroundColor: devColors.white,
    marginBottom: 20,
    marginTop: 20,
    // justifyContent: "center",
  },
  monthSeparator: {
    height: 50,
    marginTop: 10,
    width: "100%",
    justifyContent: "center",
  },
  monthSeparatorText: {
    flex: 1,
    fontSize: 20,
    fontWeight: "normal",
    textAlign: "left",
    paddingLeft: 20,
    backgroundColor: devColors.red,
    // justifyContent: "center",
  },
  monthTitle: {
    width: 60,
    justifyContent: "center",
    alignItems: "center",
  },
  monthTitleText: {
    fontSize: 16,
    justifyContent: "center",
    alignItems: "center",
  },
  droidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? 2 : 0,
  },
});

// TODO: remove dev elements
