import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create ({
    container: {
        backgroundColor: '#FFF',
        flex:1,
        alignItems: 'center'
    },
    inputView: {
        width: "80%",
        marginTop: "5%",
        alignItems: 'center',
    },
    image: {
        marginTop: "20%",
        maxHeight: "30%",
        maxWidth: "80%",
        alignItems: 'center',
        justifyContent: 'center',
    },
    teksti: {
        fontSize: 50,
        maxHeight: "40%",
        maxWidth: "100%"
    },
    buttonDivider: {
        marginBottom: "2%",
        marginTop: "2%"
    },
    registerButtonStyle: {
        marginTop: "15%"
    },
    loginButtons: {
        flex:1,
        flexDirection:"row",
        marginTop:30,
        justifyContent:"space-between"
    }
});

