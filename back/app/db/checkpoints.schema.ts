import { Checkpoint } from '../models/db/checkpoint.interfaces';
import mongoose, { Schema } from 'mongoose';

const checkpointSchema: Schema<any> = new mongoose.Schema({
  event: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Event',
    required: true,
  },
  longitude: { type: Number, required: true },
  latitude: { type: Number, required: true },
  date: { type: Date, required: true },
});

checkpointSchema.set('toJSON', {
  transform: (document, ret) => {
    ret.id = ret._id.toString();
    delete ret._id;
    delete ret.__v;
  },
});

export const Checkpoints = mongoose.model('Checkpoint', checkpointSchema);
