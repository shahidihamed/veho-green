import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create ({
    container: {
        backgroundColor: '#FFF',
        flex:1,
        alignItems: 'center'
    },
    inputView: {
        width: "80%",
        marginTop: "5%"
    },
    image: {
        marginTop: "20%",
        maxHeight: "30%",
        maxWidth: "80%"
    },
    buttonDivider: {
    },
    registerButtonStyle: {
        marginTop: "15%",
    },
    loginButtonStyle: {
        marginTop: "2%"
    },
});

