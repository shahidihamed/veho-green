import mongoose, { Schema } from 'mongoose';

const carSchema: Schema<any> = new mongoose.Schema({
  users: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
  fuelType: { type: String, required: true },
  carType: { type: String, required: true },
  registrationNumber: { type: String, required: true },
});

carSchema.set('toJSON', {
  transform: (document, ret) => {
    ret.id = ret._id.toString();
    delete ret._id;
    delete ret.__v;
  },
});

export const Cars = mongoose.model('Car', carSchema);
