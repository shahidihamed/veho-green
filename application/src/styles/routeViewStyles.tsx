import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  ecoPoints: {
    justifyContent: "center",
    alignItems: "center",
    height: "20%",
    backgroundColor: "green",
  },
  points: {
    fontSize: 35,
    color: "white",
    marginTop: "15%",

  },
  list: {
    paddingLeft: "5%",
  },
  home: {
    marginTop: "10%",
  },
  header: {
    fontSize: 35,
  },
  smallHeader: {
    fontSize: 25,
  },
  imageSlot: {
    width: 30,
    height: 50,
  },
  content: {
    flex: 1,
    height:"100%",
    borderRadius: 15,
  },
  fromContainer: {
    width: "80%",
  },
  toContainer: {
    width: "80%",
  },
  inputStyle: {
    width: "30%",
  },
  from: {},
  to: {},
  buttonContainer: {
    width: "100%",
    alignItems: "center",
  },
  button: { width: "80%" },
  calculatedData: {
      width: "100%",
      flex: 1,
      justifyContent: "space-around",
      flexDirection: "row",
      paddingBottom: 100
    }
});
