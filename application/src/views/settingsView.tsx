import * as React from "react";
import { View, Text, Switch } from "react-native";
import { ListItem, Icon } from "react-native-elements";
import { FlatList, ScrollView } from "react-native-gesture-handler";


/**
 * 
 * Settings View, currently not in used
 */

 
const sections = [
  {
    title: "Dark Mode",
    hideChevron: true,
    checkbox: true,
  }
];

const settingsView = () => {
  const [isEnabled, setIsEnabled] = React.useState<boolean>(false);
  const toggleSwitch: Function = () => 
  {
    setIsEnabled(isEnabled ? false : true);
  }

  return (
    <>
      <FlatList
        data={sections}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }: any) => (
          <ListItem containerStyle={{ paddingVertical: 8 }} key={item.title}>
            <Icon
              type="ionicon"
              name={item.icon}
              size={20}
              color="white"
              containerStyle={{
                backgroundColor: item.backgroundColor,
                width: 28,
                height: 28,
                borderRadius: 6,
                alignItems: "center",
                justifyContent: "center",
              }}
            />
            <ListItem.Content>
              <ListItem.Title>{item.title}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Content right>
              <ListItem.Title right>{item.rightTitle}</ListItem.Title>
            </ListItem.Content>
            {!item.hideChevron && <ListItem.Chevron />}
            {item.checkbox && <Switch value={isEnabled} onValueChange={() => toggleSwitch()}/>}
          </ListItem>
        )}
      >
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <Text>Settings</Text>
        </View>
      </FlatList>
    </>
  );
};

export default settingsView;
